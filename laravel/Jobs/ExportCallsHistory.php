<?php

namespace App\Jobs;

use App\Models\Admin;
use App\Models\CallsHistory;
use Carbon\Carbon;
use App\Traits\HasLogFile;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class ExportCallsHistory extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, HasLogFile;
    private $logFile = 'export-call-history.log';
    private $fileName;
    private $headings = array(
        'id' => 'Serial No.',
        'uniqueid' => 'Asterisk Call ID',
        'queue' => 'queue_number',
        'queue_group' => 'Queue name',
        'call_type' => 'call_type',
        //'vcb_request' => 'vcb_request',
        //'vcb_success' => 'vcb_success',
        'username' => 'Extension',
        'fname' => 'first_name',
        'lname' => 'last_name',
        'call_date' => 'Call Date',
        'time_start' => 'Start Time',
        'time_end' => 'End Time',
        'hold_time' => 'q_seconds',
        'duration' => 'Talk Seconds',
        'wrap_time' => 'Wrap Seconds',
        'dnis' => 'DNIS',
        'callerid' => 'ANI',
        //'disposition_id' => 'disposition_code',
        //'dispositions_category' => 'disposition',
        //'disposition_name' => 'disposition Description',
        'customer_number' => 'customer_no',
        //'company' => 'company_name',
        //'entered_position' => 'Entered Position',
        //'hangup_reason' => 'Hangup reason',
    );
    public $request;
    public $agentId;
    public $createFile;
    public $offSet;
    public $timeout = 150;

    public function __construct($request, $agentId, $fileName = false, $offSet = 0)
    {
        $this->request = $request;
        $this->agentId = $agentId;
        $this->fileName = !$fileName ? "callsHistory_" . date("Y-m-d_H-i", time()) . '.csv' : $fileName;
        $this->offSet = $offSet;
        $this->createFile = $fileName == false ? true : false;
    }

    public function handle()
    {
        $this->log("========================================== Job Started ==========================================");
        $this->createFile and Storage::disk('public')->put("calls-history/" . $this->fileName, implode(",", $this->headings));
        $recordPerPage = 50;

        $records = $this->findRequested($this->request, $this->offSet, $recordPerPage);
        $filePath = dirname(dirname(__DIR__));
        $this->log(">>".$filePath);
        if (count($records) > 0  ) {
            $file=fopen($filePath.'/storage/app/public/calls-history/'.$this->fileName,'a+');
            foreach ($records as $record) {
                $row = [];
                foreach ($this->headings as $column => $heading) {
                    if ($column == 'time_start' || $column == 'time_end') {
                        $row[$column] = date('h:i A', strtotime($record->$column));
                    }
                    /*elseif ($column == 'vcb_request' || $column == 'vcb_success')
                        $row[$column] = ($record->$column) ? 'Y' : 0;*/
                    else {
                        $row[$column] = $record->$column;
                    }
                }
                //Storage::disk('public')->append("calls-history/" . $this->fileName, implode(",", $row));
                fwrite($file,PHP_EOL.implode(",", $row));

            }
            fclose($file);
            $this->log("Loading next records...");
            $nextRecords = $this->offSet + $recordPerPage;
            $this->log($this->fileName . "::" . $nextRecords . " Records Processed at ");
            dispatch(new ExportCallsHistory($this->request, $this->agentId, $this->fileName, $nextRecords));
        } else {
            $url = Storage::disk('public')->url("calls-history/" . $this->fileName);
            $this->log("==========================================");
            app('pusher')->trigger('callsHistory', 'export-completed', $url);
            $this->log("==>>> " . $this->fileName . ">>>>Export Completed ");
            $admin = Admin::where('id', $this->agentId)->first();
            if ($admin && !empty($admin->email)) {
                \Mail::send('emails.export-call-history', ['url' => $url, 'admin' => $admin], function ($m) use ($admin, $url) {
                    $m->to($admin->email, $admin->name)->subject('Export Completed: Call History');
                });
            }
        }
        $this->log("========================================== Job Completed ==========================================");
    }

    public function findRequested($offset = 0, $records = 50)
    {
        return CallsHistory::buildFindQuery($this->request)->offset($offset)->limit($records)->get();
    }

}


