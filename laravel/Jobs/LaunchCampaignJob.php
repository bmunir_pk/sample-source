<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Artisan;

class LaunchCampaignJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    private $campaignId;

    public function __construct($campaignId)
    {
        $this->campaignId = $campaignId;
        $this->onQueue('sms');
    }

    public function handle()
    {
        Artisan::call("rn:run-sms-campaign", [
            'campaign_id' => $this->campaignId
        ]);
    }
}


