<?php

namespace App\Jobs;

use App\Models\SmsMessage;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SaveSmsTones extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var SmsMessage
     */
    public $message;

    public function __construct($message)
    {
        $this->message = $message;
    }

    public function handle()
    {
        $this->message->saveTones();
        $this->message->load('tones');
        $channel = "messages-{$this->message->conversation->agent_id}";
        app('pusher')->trigger($channel, 'updated', $this->message->toArray());
    }
}


