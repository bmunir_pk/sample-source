<?php

namespace App\Jobs;

use App\Helpers\BandwidthHelper;
use App\Models\AppSetting;
use App\Models\CampaignNumber;
use App\Models\DontCallNumberList;
use App\Models\FreepbxUser;
use App\Models\PhoneNumber;
use App\Models\SmsConversation;
use App\Models\SmsMessage;
use App\Models\SmsQueue;
use App\Models\UnsubscribeSmsCampaign;
use App\RedisModels\Agent;
use App\RedisModels\SmsConversationRedis;
use App\Traits\HasLogFile;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class HandleIncomingSms
 * @package App\Jobs
 * Example data received:
 * array (
 * 'messageId' => 'm-zhfy5q5nm67nzfaqy5lsdhy',
 * 'from' => '+13186534122',
 * 'eventType' => 'sms',
 * 'text' => 'Hey there, this is a test',
 * 'time' => '2018-02-12T07:12:17Z',
 * 'to' => '+13176897120',
 * 'state' => 'received',
 * 'messageUri' => 'https://api.catapult.inetwork.com/v1/users/u-pliay7kvrar52dvcjeymcxa/messages/m-zhfy5q5nm67nzfaqy5lsdhy',
 * 'applicationId' => 'a-il423ldddjblko6tssouclq',
 * 'direction' => 'in',
 * )
 */
class HandleIncomingSms extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, HasLogFile;
    private $logFile = 'incoming-sms-handler';
    public $message;
    /**
     * @var SmsConversation | null
     */
    public $conversation = null;
    /**
     * @var Agent | null
     */
    public $agent = null;
    public $queue = null;

    public function __construct($message)
    {
        $this->message = $message;
        $this->onQueue('sms');
    }

    public function handle()
    {
        $this->log("-------------------- Start handling incoming sms --------------------");
        $sender = array_get($this->message, 'from');
        $text = array_get($this->message, 'text');
        $queueNum = array_get($this->message, 'to');
        $this->message['date_time'] = SmsMessage::getMessageDateTime($this->message);
        $this->queue = SmsQueue::findQueue($queueNum);
        $this->queue = $this->queue ? $this->queue : $this->getFBQueue($queueNum);
        $sender = $sender != '+923336109665' ? $sender : str_replace('+92', '', $sender);

        if (!$this->queue) {
            // may have been sent directly to an agent
            $agent = $this->getUser($queueNum);
            if (!$agent) {
                $this->log("Queue or agent not found for {$queueNum}");
                return;
            }

            $this->conversation = numberQuery(SmsConversation::query(), 'customer_number', $sender)
                ->where('agent_id', $agent->id)
                ->where('needs_disposition', '0')
                ->first();
            // if this is an ongoing conversation
            if ($this->conversation) {
                // just save the message
                $msg = $this->saveSms();
                $this->notifyNewMsg($msg, $this->conversation->agent_id);
                $this->handleDncRequest();
                return;
            } else {
                $this->conversation = new SmsConversation();
                $this->conversation->started_at = $this->getDateTime();
            }

            $this->conversation->agent_id = $agent->id;
            $this->conversation->username = $agent->username;
            $this->conversation->agent_name = $agent->first_name . " " . $agent->last_name;
            $this->conversation->customer_number = $sender;
            $this->conversation->status = 'Ongoing';
            $this->conversation->display_status = 'Received';
            $this->conversation->needs_disposition = 0;
            $this->conversation->save();
            // save sms
            $this->saveSms();
            $this->notifyNewConversation();
            $this->handleDncRequest();
            return;
        }

        // find any ongoing conversation for the sender
        $query = SmsConversation::where('queue_id', $this->queue->id);
        if ($this->queue->type == 'DID') {
            $query = numberQuery($query, 'customer_number', $sender);
        } else {
            $query->where('customer_number', $sender);
        }
        $this->conversation = $query->orderBy('id', 'desc')->first();

        // if sms_conversation is (Completed or Dispositioned) and customer_rating is null and the sms text length is 1 char and sms text is an integer bw 1-5
        // consider the message as rating message, save integer value to customer_rating field
        if ($this->isRatingMessage()) {

            $this->conversation->customer_rating = intval($text);
            $this->conversation->save();
            //$msg = $this->saveSms();
            //$this->notifyNewMsg($msg, $this->conversation->agent_id);
            $this->conversation->sendClosingMessage();
            return;
        }

        // if this is an ongoing conversation
        if ($this->conversation && !$this->isConversationRated() && !$this->isConversationCompleted()) {
            // just save the message
            $msg = $this->saveSms();
            $this->handleCampaignConversation();
            $this->notifyNewMsg($msg, $this->conversation->agent_id);
            $this->afterSaveQueueMessage();
            return;
        }

        // if no conversation is going on for the sender
        if($this->queue && $this->queue->type == 'Whatsapp') {
            $queueIcon = 'whatsapp';
        }
        else if($this->queue && $this->queue->type == 'Facebook') {
            $queueIcon = 'facebook';
        }
        else {
            $queueIcon = 'phone';
        }
        // create sms_conversation
        $this->conversation = new SmsConversation();
        $this->conversation->queue = $queueNum;
        $this->conversation->queue_id = $this->queue->id;
        $this->conversation->customer_number = $sender;
        $this->conversation->needs_disposition = 1;
        $this->conversation->started_at = $this->getDateTime();
        $this->conversation->status = 'Waiting for agent';
        $this->conversation->display_status = 'Waiting for agent';
        $this->conversation->queue_icon = $queueIcon;
        $this->conversation->save();
        // save sms
        $this->saveSms();

        $this->afterSaveQueueMessage();

        $this->notifyNewConversation();

        $this->log("--------------------- end handling incoming sms ---------------------");
    }

    public function getDateTime()
    {
        return SmsMessage::getMessageDateTime($this->message);
    }

    private function notifyNewConversation()
    {
        $this->conversation->load(
            array('messages' => function ($query) {
                $query->with(['attachments', 'tones']);
            })
        );
        $conversation = $this->conversation->toArray();
        $conversation['messages'] = collect($conversation['messages'])->groupBy('date');
        app('pusher')->trigger('conversations', 'new', $conversation);
    }

    private function notifyNewMsg($msg, $agentId = null)
    {
        if ($agentId) {
            app('pusher')->trigger('messages-' . $agentId, 'new', $msg);
        }
    }

    private function isConversationCompleted()
    {
        return $this->conversation && in_array($this->conversation->status, ['Completed', 'Dispositioned']);
    }

    private function isConversationRated()
    {
        return $this->conversation && intval($this->conversation->customer_rating) > 0;
    }

    private function isRatingMessage()
    {
        $text = array_get($this->message, 'text');
        return $this->isConversationCompleted() && !$this->isConversationRated() && strlen($text) == 1 && intval($text) > 0 && intval($text) < 6;
    }

    private function saveSms()
    {
        $message = SmsMessage::createMessage($this->conversation->id, $this->message);
        if (array_get($this->message, 'eventType') == 'mms') {
            $bandwidth = new BandwidthHelper();
            $messageAttachment = [];
            foreach (array_get($this->message, 'media') as $media) {
                if (ends_with($media, '.txt')) {
                    //This file include message details and we only want attachments
                    continue;
                }
                $bwMedia = $bandwidth->getMedia($media);
                $fileName = array_get($bwMedia, 'file_name');
                $messageAttachment[] = [
                    'file_name' => $fileName,
                    'file_ext' => extFromFileName($fileName),
                    'file_size' => array_get($bwMedia, 'file_size'),
                    'path' => $media,
                    'source' => 'Bandwidth',
                ];
            }
            if (count($messageAttachment)) {
                $message->attachments()->createMany($messageAttachment);
                $message->load(['attachments', 'tones']);
            }
        }
        return $message;
    }

    private function getUser($did)
    {
        return numberQuery(FreepbxUser::query(), 'did_for_sms', $did)->first();
    }

    private function handleDncRequest()
    {
        $text = array_get($this->message, 'text');
        if (strtolower($text) == 'stop') {
            $sender = array_get($this->message, 'from');
            $this->logDncRequest("DNC request received from {$sender}");
            $campaignNumber = CampaignNumber::where('conversation_id', $this->conversation->id)->first();
            if ($campaignNumber) {
                $this->log("Number belongs to campaign id: {$campaignNumber->campaign_id}.");
            }
            $this->logDncRequest("Adding to DNC");
            DontCallNumberList::addNumber($sender, $this->conversation->agent_id);
        }
    }

    private function afterSaveQueueMessage()
    {
        $this->handleDncRequest();
        $this->conversation->sendAfterHourMessage();
    }

    private function logDncRequest($content)
    {
        logContent($content, 'dnc-request-' . date('Y-m-d') . '.log');
    }

    private function handleCampaignConversation()
    {
        if (!$this->conversation->campaign_id) return;
        $redisConversation = SmsConversationRedis::find($this->conversation->id);
        if (!$redisConversation) return;
        if ($redisConversation->customer_reply) return;
        $phoneNumberIds = numberQuery(PhoneNumber::query(), 'number', $this->conversation->customer_number)
            ->select('id')
            ->get();
        if (!$phoneNumberIds->count()) return;
        $number = CampaignNumber::whereCampaignId($this->conversation->campaign_id)
            ->whereIn('number_id', $phoneNumberIds->pluck())
            ->first();
        if (!$number) return;
        $redisConversation->customer_reply = $number->customer_reply = 1;
        $number->save();
        $redisConversation->save(); //This will save number of queries every time when customer reply
    }

    private function getFBQueue($number) {
        $query = SmsQueue::query();
        $query->where('number', $number);
        return $query->first();
    }
}


