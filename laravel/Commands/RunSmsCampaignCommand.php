<?php

namespace App\Console\Commands;

use App\Helpers\CampaignHelper;
use App\Models\Campaign;
use App\Models\CampaignAttachment;
use App\Models\CampaignNumber;
use App\Models\DontCallNumberList;
use App\Models\PhoneNumber;
use App\Models\SmsConversation;
use App\Models\SmsMessage;
use App\Models\SmsMessageAttachment;
use App\Models\SmsQueue;
use App\Models\UnsubscribeSmsCampaign;
use App\RedisModels\Agent;
use Illuminate\Console\Command;
use App\Traits\HasLogFile;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;

class RunSmsCampaignCommand extends Command
{
    use HasLogFile;

    protected $signature = 'rn:run-sms-campaign {campaign_id?}';
    protected $description = 'checks sms campaigns if there is any and send messages';
    protected $logDir = "sms-campaign-command";
    private $numberOfBulkMessages = 10;
    private $messages = [];
    private $errors = [];

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->log("--------------------------Command Called");
        $now = time();
        $today = date("Y-m-d", $now);
        $currTime = date("H:i:s", $now);
        $campaigns = Campaign::where('start_date', $today)
            ->where('start_time', '<=', $currTime)
            ->where('is_sms_campaign', 1)
            ->where('status', 1);
        if ($campaignId = $this->argument('campaign_id')) {
            $this->log("Requested by campaign id: {$campaignId}");
            $campaigns->where('id', '=', $campaignId);
        }
        $campaigns = $campaigns->select('id', 'sms_queue', 'message_text')
            ->with('attachments')
            ->get();

        if ($campaigns->count()) {
            $queues = [];
            foreach ($campaigns as $campaign) {
                $queue = array_get($queues, $campaign->sms_queue);
                if (!$queue) {
                    $queue = SmsQueue::findQueue($campaign->sms_queue);
                    if ($queue) {
                        $queues[$campaign->sms_queue] = $queue;
                    }
                }
                if (!$queue) {
                    $this->log("Queue not found.");
                    continue;
                }
                if ($this->getInProgress($campaign->sms_queue)) {
                    //Don't run same queue campaign (another process may be start before complete this one)
                    $this->log("Campaign already running in queue {$campaign->sms_queue}");
                    continue;
                }
                $this->setInProgress($campaign->sms_queue, $campaign->id);
                try {
                    $this->processCampaign($queue, $campaign);
                    $this->updateCampaign($campaign);
                    $this->saveMessages($campaign->attachments);
                } catch (\Exception $exception) {
                    $this->log("Exception running campaign: " . toStr($exception->getTraceAsString()));
                }
                $this->removeInProgress($campaign->sms_queue, $campaign->id);
                app('pusher')->trigger('campaigns', "summary-{$campaign->id}", CampaignHelper::smsCampaignSummary($campaign));
            }
        }
        //Call sms status update command
        Artisan::call('rn:trace-sms-status');
    }

    /**
     * @param $campaign Campaign
     */
    private function updateCampaign($campaign)
    {
        if (array_key_exists($campaign->id, $this->errors)) {
            $campaign->error_message = $this->errors[$campaign->id];
            if (str_contains($campaign->error_message, 'contains a blocked NPA code')) {
                if (preg_match("/'(.*?)'/", $campaign->error_message, $match) == 1) {
                    $number = array_get($match, 1);
                    if ($number) {
                        $this->log("Match: {$number}");
                        $phoneNumbers = numberQuery(PhoneNumber::query(), 'number', $number)->get();
                        if ($phoneNumbers->count()) {
                            $this->log("Number found: " . toStr($phoneNumbers));
                            $campaignNumber = CampaignNumber::whereCampaignId($campaign->id)
                                ->whereIn('number_id', $phoneNumbers->pluck('id')->toArray())->first();
                            $campaignNumber->sms_status = "Message Failed";
                            $campaignNumber->comments = "Failed: " . $campaign->error_message;
                            $campaignNumber->save();
                        }
                    }

                }
            }
        }
        if (!CampaignNumber::whereCampaignId($campaign->id)->whereNull('sms_status')->count()) {
            $campaign->status = 0;
            $campaign->completed_at = date('Y-m-d H:i:s');
        }
        $campaign->save();
    }

    /**
     * @param $queue SmsQueue
     * @param $campaign Campaign
     */
    private function processCampaign($queue, $campaign)
    {
        $campaignNumbers = CampaignNumber::where('campaign_id', $campaign->id)
            ->whereNull('sms_status')
            ->join('phone_numbers', 'phone_numbers.id', '=', 'campaign_numbers.number_id')
            ->select('campaign_numbers.number_id', 'campaign_numbers.id as id', 'phone_numbers.number')
            ->get();

        if (!$campaignNumbers->count()) {
            $this->log("No more numbers.");
            return;
        }
        $numbers = [];
        $campaignNumberIds = [];
        foreach ($campaignNumbers as $number) {
            $num = number_to_e164(stripNumber($number->number));
            $numbers[$number->number_id] = $num;
            $campaignNumberIds[$number->number_id] = $number->id;
        }

        $numbers = $this->filterUnSubscribedNumbers($numbers, $campaign->id);
        if (!count($numbers)) {
            $this->log("No more number. After DNC");
            return;
        }
        $messages = [];
        $attachments = [];
        if ($campaign->attachments && count($campaign->attachments)) {
            $attachments = $campaign->attachments->pluck('path')->toArray();
        }
        $this->log("Campaign attachments: ".toStr($attachments));
        $text = $campaign->message_text;
        $containDynamicFields = Str::contains($text, '[');
        foreach ($numbers as $numberId => $number) {
            if ($containDynamicFields) {
                $text = SmsMessage::textTransformation($campaign->message_text, $number, $queue->number, $campaignNumberIds[$numberId]);
            }
            $message = [
                'to' => $number,
                'from' => $queue->number,
                'text' => $text,
            ];

            if (count($attachments)) {
                $message['media'] = $attachments;
            }
            $messages[] = $message;
            /*
             * There is a maximum limit of 50 messages sent per multiple messages request.
             * https://dev.bandwidth.com/ap-docs/methods/messages/postMessages.html#multiple-message-result-types
             * */
            if (count($messages) == $this->numberOfBulkMessages) {
                $this->log("Sending {$this->numberOfBulkMessages} messages");
                $this->sendMultipleMessages($messages, $queue, $campaign->id, $numbers);
                $messages = [];
                if (count($this->messages) == 50) {
                    $this->saveMessages($campaign->attachments);
                }
                sleep(($this->numberOfBulkMessages + 1)); //Only sleep after messages send
            }
        }
        if (count($messages)) {
            $this->sendMultipleMessages($messages, $queue, $campaign->id, $numbers);
            $this->saveMessages($campaign->attachments);
            sleep(($this->numberOfBulkMessages + 1)); //May be another campaign runs after this without complete avg rate
        }
    }

    /**
     * @param $numbers array
     * @param $campaignId integer
     * @return  array
     */
    private function filterUnSubscribedNumbers($numbers, $campaignId)
    {
        $dontCallNumbers = DontCallNumberList::whereIn('number_id', array_keys($numbers))
            ->select('number_id')
            ->get();
        if ($dontCallNumbers->count()) {
            $dontCallNumberIds = [];
            foreach ($dontCallNumbers as $dontCallNumber) {
                $dontCallNumberIds[] = $dontCallNumber->number_id;
                unset($numbers[$dontCallNumber->number_id]);
            }
            $this->log("Adding numbers(number_id) to DNC: " . toStr($dontCallNumberIds));
            CampaignNumber::where('campaign_id', '=', $campaignId)
                ->whereIn('number_id', $dontCallNumberIds)
                ->update([
                    'sms_status' => 'DNC - Not Sent',
                    'do_not_call' => 1,
                ]);
        }
        return $numbers;
    }

    /**
     * @param $campaignIds array
     * @return  boolean
     */
    private function pauseCampaigns($campaignIds)
    {
        return Campaign::whereIn('id', $campaignIds)->update(['status' => 0]);
    }

    /**
     * @param $messages array
     * @param $queue SmsQueue
     * @param $campaignId integer
     * @param $numbers array
     */
    private function sendMultipleMessages($messages, $queue, $campaignId, $numbers)
    {
        $this->log("Sending messages: " . count($messages));
        $startTime = date('Y-m-d H:i');
        $apiMsgs = $this->sendMessagesToApi($messages);
        $this->log("Response of bulk sms: " . toStr($apiMsgs));
        if (!$apiMsgs) return;
        $apiMsgs = collect($apiMsgs)->groupBy('result');
        if ($errors = $apiMsgs->get('error')) {
            $this->log("Errors: " . toStr($errors));
            if (!array_key_exists($campaignId, $this->errors) && $firstError = array_get($errors, '0.error.message')) {
                $this->errors[$campaignId] = $firstError;
            }
        }
        $apiMsgIds = $this->getBulkMessageIds($apiMsgs->get('accepted'));
        $this->log("apiMsgIds: " . toStr($apiMsgIds));

        if (!count($apiMsgIds)) {
            return;
        }
        $apiMsgs = $this->getMessagesFromApi([
            'from' => number_to_e164($queue->number),
            'fromDateTime' => $startTime,
        ]);
        foreach ($apiMsgs as $apiMsg) {
            $apiMsgIdKey = array_search(array_get($apiMsg, 'messageId'), $apiMsgIds);
            if ($apiMsgIdKey === false) {
                $this->log("Does not belong to last request." . toStr($apiMsg));
                continue;
            }
            $this->saveConversation($apiMsg, $queue, $campaignId, $numbers);
            //Remove saved message id
            unset($apiMsgIds[$apiMsgIdKey]);
            if (!count($apiMsgIds)) {
                //No more message left
                $this->log("All messages saved. Skip rest of messages.");
                return;
            }
        }
        if (count($apiMsgIds)) {
            $this->log("Some messages not fetched somehow: " . toStr($apiMsgIds));
            foreach ($apiMsgIds as $apiMsgId) {
                $apiMsg = app('smsApi')->getMessageById($apiMsgId);
                $this->saveConversation($apiMsg, $queue, $campaignId, $numbers);
            }
        }
    }

    /**
     * @param $apiMsg array
     * @param $queue SmsQueue
     * @param $campaignId integer
     * @param $numbers &array
     */
    private function saveConversation($apiMsg, $queue, $campaignId, &$numbers)
    {
        $to = number_to_e164(array_get($apiMsg, 'to'));
        $numberId = array_search($to, $numbers);
        if (!$numberId) {
            $numberId = array_search(ltrim($to, '+1'), $numbers); //Let's give try without leading +1
            if (!$numberId) {
                $this->log("cannot find number id: {$to}");
                $this->log("Numbers: " . toStr($numbers));
                return;
            }
        }
        unset($numbers[$numberId]);
        $this->markOldConversationAsComplete($queue->id, array_get($apiMsg, 'to'));
        $apiMsg['date_time'] = SmsMessage::getMessageDateTime($apiMsg);
        $conversation = new SmsConversation();
        $conversation->started_at = $apiMsg['date_time'];
        $conversation->queue = $queue->number;
        $conversation->queue_id = $queue->id;
        $conversation->customer_number = array_get($apiMsg, 'to');
        $conversation->status = 'Waiting for agent';
        $conversation->display_status = 'Waiting for agent';
        $conversation->needs_disposition = 1;
        $conversation->num_messages = 1;
        $conversation->campaign_id = $campaignId;
        $conversation->save();
        $this->log("SmsConversation: {$conversation->id}");
        $this->push($conversation->id, $apiMsg);
        $smsStatus = $status = "Message " . ucfirst($apiMsg['state']);;

        CampaignNumber::where([
            ['number_id', '=', $numberId],
            ['campaign_id', '=', $campaignId],
        ])->update([
            'conversation_id' => $conversation->id,
            'sms_status' => $smsStatus,
            'date_time' => $apiMsg['date_time'],
        ]);
    }

    /**
     * @param $queueId integer
     * @param $to string
     */
    private function markOldConversationAsComplete($queueId, $to)
    {
        /* @var $conversation SmsConversation */
        $conversation = numberQuery(SmsConversation::query(), 'customer_number', $to)
            ->where('queue_id', '=', $queueId)
            ->where('num_messages', '<=', 1)
            ->whereIn('status', ['Waiting for agent', 'Ongoing'])
            ->orderBy('updated_at', 'desc')
            ->first();
        if ($conversation) {
            $this->log("Marking {$conversation->id} as complete");
            $conversation->status = "Completed";
            $conversation->completed_at = date('Y-m-d H:i:s');
            $conversation->save();
            if ($conversation->username) {
                $key = Agent::agentConversationSet($conversation->username);
                $this->log("Removing conversation from agent set: {$key}");
                app('redis')->srem($key, $conversation->id);
            }
        }
    }
    /**
     * @param $conversationId integer
     * @param $message array
     */
    private function push($conversationId, $message)
    {
        $currentTime = array_get($message, 'date_time');
        $this->messages[] = [
            'sms_conversation_id' => $conversationId,
            'from' => array_get($message, 'from'),
            'to' => array_get($message, 'to'),
            'text' => array_get($message, 'text'),
            'direction' => array_get($message, 'direction'),
            'api_message_id' => array_get($message, 'messageId'),
            'api_message_state' => array_get($message, 'state'),
            'api_message_uri' => array_get($message, 'messageUri', 'n/a'),
            'read' => 1,
            'date_time' => $currentTime,
            'created_at' => $currentTime,
            'updated_at' => $currentTime,
        ];
    }

    /**
     * @param $params array
     * @return array
     */
    private function getMessagesFromApi($params)
    {
        $params['size'] = 200; //May be some other messages are already on bandwidth
        return app('smsApi')->getMessages($params);
    }

    private function sendMessagesToApi($messages)
    {
        return app('smsApi')->createMultipleMessages($messages);
    }

    private function getBulkMessageIds($apiMsgs)
    {
        $apiMsgIds = [];
        if ($apiMsgs) {
            foreach ($apiMsgs as $apiMsg) {
                $messageId = $this->getMsgId(array_get($apiMsg, 'location'));
                if ($messageId) $apiMsgIds[] = $messageId;
            }
        }
        return $apiMsgIds;
    }

    private function getMsgId($uri)
    {
        $arr = explode('/', $uri);
        return end($arr);
    }

    /**
     * @param $attachments []
     * */
    private function saveMessages($attachments)
    {
        //create all messages at once
        if (count($this->messages)) {
            $this->log("Messages to save: " . toStr($this->messages));
            try {
                if (count($attachments)) {
                    $messageAttachments = [];
                    foreach ($this->messages as $message) {
                        $smsMessage = SmsMessage::create($message);
                        foreach ($attachments as $attachment) {
                            /* @var $attachment CampaignAttachment */
                            $messageAttachments[] = [
                                'sms_message_id' => $smsMessage->id,
                                'path' => $attachment->path,
                                'file_name' => $attachment->file_name,
                                'file_size' => $attachment->file_size,
                                'file_ext' => $attachment->file_ext,
                                'source' => $attachment->source,
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s'),
                            ];
                        }
                    }
                    if ($messageAttachments) {
                        SmsMessageAttachment::insert($messageAttachments);
                    }
                } else {
                    SmsMessage::insert($this->messages);
                }
                $this->messages = [];
            } catch (\Exception $exception) {
                $this->log("Error saving messages." . $exception->getMessage());
                $this->log($exception->getTraceAsString());
            }

        }
    }

    private function getProgressTag($number)
    {
        return "smsQueueCampaign:{$number}";
    }

    private function getInProgress($number)
    {
        return app('redis')->get($this->getProgressTag($number));
    }

    private function setInProgress($number, $campaignId)
    {
        app('pusher')->trigger('campaigns', "running", $campaignId);
        return app('redis')->set($this->getProgressTag($number), $campaignId);
    }

    private function removeInProgress($number, $campaignId)
    {
        app('pusher')->trigger('campaigns', "complete", $campaignId);
        return app('redis')->del($this->getProgressTag($number));
    }
}
