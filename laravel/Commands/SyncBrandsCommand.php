<?php

namespace App\Console\Commands;

use App\Helpers\SyncBrandsHelper;
use App\Models\AsteriskEndPointBrand;
use App\Models\DmBrand;
use App\Traits\HasLogFile;
use Illuminate\Console\Command;

class SyncBrandsCommand extends Command
{
    use HasLogFile;
    protected $logFile = 'sync-brand-command';
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rn:sync-brands-command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        SyncBrandsHelper::runSync();
    }
}
