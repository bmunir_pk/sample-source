<?php

namespace App\Console\Commands;

use App\Helpers\ScheduledReports\ScheduledReportHelper;
use App\Helpers\ScheduledReports\ScheduledReportTypeHelper;
use App\Models\ScheduledReport;
use App\Models\ScheduledReportDay;
use App\Models\ScheduledReportFilteredCampaign;
use App\Models\ScheduledReportFilteredDay;
use App\Models\ScheduledReportFilteredQueue;
use App\Traits\HasLogFile;
use Illuminate\Console\Command;

class SendScheduledReportCommand extends Command
{
    use HasLogFile;
    protected $signature = 'rn:send-scheduled-reports';
    protected $description = 'Send active scheduled report';
    protected $logFile = "send-scheduled-report-command";
    private $reportsToSend;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->log("================================= Handling Schedule Reports =================================");
        $this->init();
        if (!$this->reportsToSend->count()) {
            $this->log('No report to send for now');
            return;
        }
        $this->send();              //Send all reports
    }

    private function init()
    {
        $this->reportsToSend = collect([]);
        $reports = ScheduledReport::whereStatus('Active')->get();
        if (!$reports->count()) {
            $this->log('Active reports not found on the server');
            return;
        }
        foreach ($reports as $report) {
            if ($currentDaySetting = $this->reportNeedsToBeSent($report)) {
                $this->reportsToSend->push($report);    //Get only reports that needs to send
                if ($this->isTwiceADayReport($report->type)) {
                    if ((int)date("H") <= (int)date('H', strtotime($currentDaySetting->start_time))) {
                        //Swap start and end time in case report is generated before start time
                        $report->start_time = $currentDaySetting->end_time;
                        $report->end_time = $currentDaySetting->start_time;
                        continue;
                    }
                    $report->start_time = $currentDaySetting->start_time;
                    $report->end_time = $currentDaySetting->end_time;
                }
            }
        }
    }

    private function reportNeedsToBeSent($report)
    {
        // current day setting
        $reportCurrentDaySetting = ScheduledReportDay::where([
            ['scheduled_report_id', $report->id],
            ['report_day', date('l')]
        ])->first();

        if (!$reportCurrentDaySetting->status) {
            $this->log("Current day is not enabled for Report: {$report->name} ({$report->type})");
            return false;
        }

        // current time should be greater than $timeForFirstReport
        $timeForFirstReport = strtotime(date('Y-m-d ') . $reportCurrentDaySetting->start_time);
        $currentTime = time();
        if ($currentTime < $timeForFirstReport) {
            $this->log("Start time is less then first report for  Report: {$report->name} ({$report->type})");
            return false;
        }

        // current time should be less than $timeForLastReport
        $timeForLastReport = strtotime(date('Y-m-d ') . $reportCurrentDaySetting->end_time);
        if ($currentTime > $timeForLastReport) {
            $this->log("End time is greater then last report for  Report: {$report->name} ({$report->type})");
            return false;
        }

        // current hour should be a multiple of frequency
        $currentHour = date("G");
        $firstHour = date("G", $timeForFirstReport);
        $diff = $currentHour - $firstHour;
        if ($diff % $report->frequency) {
            $this->log("Time does not match the report frequency for  Report: {$report->name} ({$report->type})");
            return false;
        }

        // email should not be already sent
        $lastEmail = $this->getLastEmailTime($report->id);
        if ($this->currentEmailTime() == $lastEmail) {
            $this->log(" Report: {$report->name} ({$report->type}) Email is already sent.");
            return false;
        }

        return $reportCurrentDaySetting;
    }

    private function send()
    {
        foreach ($this->reportsToSend as $report) {
            $this->setLastEmailTime($report->id, $this->currentEmailTime());
            (new ScheduledReportHelper($this->getReportParams($report)))->send($report);
        }
    }

    private function currentEmailTime()
    {
        return date('Y-m-d H:00:00');
    }

    private function getLastEmailTime($reportId)
    {
        return app('redis')->get($this->redisName($reportId));
    }

    private function setLastEmailTime($reportId, $time)
    {
        app('redis')->set($this->redisName($reportId), $time);
    }

    private function redisName($reportId)
    {
        return "last-report-email-time-{$reportId}";
    }

    private function isTwiceADayReport($type)
    {
        return str_contains($type, 'Twice a Day');
    }

    /**
     * @param $report ScheduledReport
     * @return array
    */
    private function getReportParams($report)
    {
        $today = date("Y-m-d");
        $config = [
            'startDate' => $today,
            'endDate' => $today,
        ];
        $reportType = ScheduledReportTypeHelper::find($report->type);
        if (array_get($reportType, 'filter.time')) {
            $config['startTime'] = $report->start_time;
            $config['endTime'] = strtotime($report->end_time) < time() ? $report->end_time : date('H:i:s', time());
            //Modify date time in case of twice a day report
            if (strtotime($config['startTime']) > strtotime($config['endTime'])) {
                $config['startDate'] = date('Y-m-d', strtotime("-1 days"));
            }
        }
        if (array_get($reportType, 'filter.days')) {
            $config['days'] = $this->getFilterDays($report->id);
        }
        if (array_get($reportType, 'filter.queues') && $report->filter_by_queues) {
            $config['queues'] = $this->getFilterQueues($report->id);
        }
        if (array_get($reportType, 'filter.campaigns')) {
            $config['campaigns'] = $this->getFilterCampaigns($report->id);
        }
        if (array_get($reportType, 'filter.location')) {
            $config['location_id'] = $report->location_id;
        }

        return $config;
    }

    private function getFilterQueues($reportId)
    {
        return ScheduledReportFilteredQueue::whereScheduledReportId($reportId)
            ->select('queue')
            ->pluck('queue')
            ->toArray();
    }

    private function getFilterDays($reportId)
    {
        return ScheduledReportFilteredDay::whereScheduledReportId($reportId)
            ->select('filtered_day')
            ->pluck('filtered_day')
            ->toArray();
    }

    private function getFilterCampaigns($reportId)
    {
        return ScheduledReportFilteredCampaign::whereScheduledReportId($reportId)
            ->select('campaign_id')
            ->pluck('campaign_id')
            ->toArray();
    }
}
