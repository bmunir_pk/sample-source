<?php

namespace App\Console\Commands;

use App\RedisModels\MissedCalls;
use App\Traits\HasLogFile;
use Illuminate\Console\Command;

class SaveMissedCallsToDB extends Command
{
    use HasLogFile;
    protected $logFile = "save-missed-calls-to-db";

    protected $signature = 'rn:save-missed-calls-to-db';
    protected $description = 'Clears missed calls from redis and Saves them to database.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $keys = app('redis')->keys("missed-calls:*");
        $this->log("Calls found: " . count($keys));
        $data = [];
        foreach ($keys as $key) {
            $call = MissedCalls::find(str_after($key, ':'));
            $data[] = $call->getDbAttributes();
            $call->delete();
            if (count($data) > 9) {
                $this->log("Inserting data: " . var_export($data, true));
                \DB::table('missed_calls')->insert($data);
                $data = [];
            }
        }

        if (!empty($data)) {
            $this->log("Inserting data: " . var_export($data, true));
            \DB::table('missed_calls')->insert($data);
        }
    }
}
