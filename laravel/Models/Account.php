<?php

namespace App\Models;

use App\Traits\InputSanitizer;
use function foo\func;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
    use InputSanitizer;
    use SoftDeletes;

    protected $fillable = ['account_name','address1', 'address2', 'city', 'state', 'zip', 'country_id', 'doj', 'state_of_corp'];

    public static $rules = [
        'account_name'=>'required|unique:accounts',
        'address1'=>'required',
        'city'=>'required',
        'state'=>'required',
        'zip'=>'required',
        'country_id'=>'required'
    ];

    public static function findRequested ($count=false) {
        $pagesize = \Request::Input('pageSize');
        $currentPage = \Request::Input('currentPage');
        $filter = \Request::Input('filter');
        $q = \Request::Input('q');

        $query = self::whereNotNull('id');
        $q and $query->where(function ($query) use ($q) {
            return $query->where('account_name', 'like', '%'.$q.'%')
                        ->orWhere('address1', 'like', '%'.$q.'%')
                        ->orWhere('city', 'like', '%'.$q.'%');
        });
        $query->with(['contacts'=> function($query){
            return $query->orderBy('is_primary', 'DESC');
        }]);
        $query->with('projects');
        if($count) {
            return $query->count();
        }
        $query->skip($pagesize * $currentPage)->take($pagesize);

        return $query->get();
    }

    public function contacts()
    {
        return $this->hasMany(AccountContact::class, 'account_id', 'id');
    }

    public function projects()
    {
        return $this->hasMany(Project::class, 'client_id', 'id');
    }

}
