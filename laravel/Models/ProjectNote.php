<?php

namespace App\Models;
use App\Traits\InputSanitizer;
use App\User;
use Illuminate\Database\Eloquent\Model;

class ProjectNote extends Model
{
    //
    use InputSanitizer;
    protected $fillable = ['user_id', 'project_id', 'notes'];

    public function added_by() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
