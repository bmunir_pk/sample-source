<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SbcPanel *
 * @property  integer $id
 * @property  integer $title
 * @property  integer $ip_address
 * @property  integer $db_name
 * @property  integer $db_user
 * @property  integer $db_password
 * @property  integer $client_portal_id
 * @property  integer $created_at
 * @property  integer $updated_at
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SbcPanel whereId($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SbcPanel whereTitle($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SbcPanel whereIpAddress($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SbcPanel whereDbName($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SbcPanel whereDbUser($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SbcPanel whereDbPassword($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SbcPanel whereClientPortalId($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SbcPanel whereCreatedAt($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SbcPanel whereUpdatedAt($value)
 */
class SbcPanel extends Model
{
    public $guarded = ["id", "created_at", "updated_at"];

    public static function findRequested()
    {
        $query = SbcPanel::query();

        // search results based on user input
        \Request::has('id') and $query->where('id',\Request::input('id'));
        \Request::has('title') and $query->where('title','like','%'.\Request::input('title').'%');
        \Request::has('host') and $query->where('host','like','%'.\Request::input('host').'%');
        \Request::has('db_name') and $query->where('db_name','like','%'.\Request::input('db_name').'%');
        \Request::has('db_user') and $query->where('db_user','like','%'.\Request::input('db_user').'%');
        \Request::has('db_password') and $query->where('db_password','like','%'.\Request::input('db_password').'%');
        \Request::has('created_at') and $query->where('created_at',\Request::input('created_at'));
        \Request::has('updated_at') and $query->where('updated_at',\Request::input('updated_at'));
        
        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"), \Request::input("sortType", "asc"));

        // paginate results
        if ($resPerPage = \Request::input("perPage"))
            return $query->paginate($resPerPage);
        return $query->get();
    }

    public static function validationRules($attributes = null)
    {
        $rules = [
            'title' => 'required|string|max:255',
            'host' => 'required|string|max:255',
            'db_name' => 'required|string|max:255',
            'db_user' => 'required|string|max:255',
            'db_password' => 'required|string|max:255',
            'api_token' => 'required|string|max:255'
        ];

        // no list is provided
        if (!$attributes)
            return $rules;

        // a single attribute is provided
        if (!is_array($attributes))
            return [$attributes => $rules[$attributes]];

        // a list of attributes is provided
        $newRules = [];
        foreach ($attributes as $attr)
            $newRules[$attr] = $rules[$attr];
        return $newRules;
    }

}
