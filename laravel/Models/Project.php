<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    //
    protected $fillable = [
        'name',
        'has_assigned',
        'pid',
        'date_of_start',
        'visibility',
        'client_id',
        'account_manager',
        'project_manager',
        'contracted_hours',
        'notes',
        'status',
        'work_order',
        'created_by'
    ];

    public static $rules = [
        'name' => 'required|unique:projects',
        'date_of_start'=>'required',
        'visibility'=>'required',
        'tags' => 'required',
        //'client_id'=>'required',
        'account_manager'=>'required',
        'project_manager'=>'required',
        'status'=>'required',
        //'work_order'=>'required'
    ];

    public function findRequested ($count=false) {
        $pagesize = \Request::Input('pageSize');
        $currentPage = \Request::Input('currentPage');
        $q = \Request::Input('q');
        $isAssigning = \Request::Input('is_assigning');

        $query = Project::with('client');
        $query->with('accountManager');
        $query->with('projectManager');
        $query->with('workOrderRow');
        $query->with(['lastUpdated' => function($query){
            $query->with('user')
                ->orderBy('id', 'DESC')
                ->first();
        }]);
        $query->with(['hoursSpent' => function($query) {
            $query->groupBy('project_id')
                ->select('project_id', \DB::raw('sum(hours) as spent_hours'));
        }]);

        $query->with(['tags'=>function($query){
            $query->join('tags', 'tags.id', '=', 'tag_id')
                ->select(\DB::raw('title as text'), 'tags.id', 'project_tags.project_id');
        }]);

        $q and $query->where(function ($query) use ($q) {
            return $query->where('name', 'like', '%'.$q.'%');
        });

        $isAssigning and $query->whereNull('client_id');

        if($count) return $query->count();

        $pagesize && $currentPage and $query->skip($pagesize * $currentPage)->take($pagesize);
        return $query->get();
    }

    public function accountManager () {
        return $this->hasOne(User::class, 'id', 'account_manager');
    }

    public function projectManager () {
        return $this->hasOne(User::class, 'id', 'project_manager');
    }

    public function client () {
        return $this->hasOne(Account::class, 'id', 'client_id')->with('contacts');
    }

    public function hours () {
        return $this->hasMany(ContractedHour::class, 'work_order_id', 'work_order');
    }

    public function workOrderRow () {
        return $this->hasOne(WorkOrder::class, 'id', 'work_order');
    }

    public function tags()
    {
        return $this->hasMany(ProjectTag::class, 'project_id', 'id');
    }

    public function lastUpdated () {
        return $this->hasMany(Projecthistory::class, 'project_id', 'id');
    }

    public function hoursSpent () {
        return $this->hasMany(HoursSpent::class, 'project_id', 'id');
    }

}
