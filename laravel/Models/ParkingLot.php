<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ParkingLot *
 * @property  integer $id
 * @property  integer $name
 * @property  integer $title
 * @property  integer $extension
 * @property  integer $created_at
 * @property  integer $updated_at
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\ParkingLot whereId($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\ParkingLot whereName($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\ParkingLot whereTitle($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\ParkingLot whereExtension($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\ParkingLot whereCreatedAt($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\ParkingLot whereUpdatedAt($value)
 */
class ParkingLot extends Model
{

    public $guarded = ["id", "created_at", "updated_at"];

    public static function findRequested()
    {
        $query = ParkingLot::query();

        // search results based on user input
        \Request::input('id') and $query->where('id', \Request::input('id'));
        \Request::input('name') and $query->where('name', 'like', '%' . \Request::input('name') . '%');
        \Request::input('title') and $query->where('title', 'like', '%' . \Request::input('title') . '%');
        \Request::input('extension') and $query->where('extension', 'like', '%' . \Request::input('extension') . '%');
        \Request::input('created_at') and $query->where('created_at', \Request::input('created_at'));
        \Request::input('updated_at') and $query->where('updated_at', \Request::input('updated_at'));

        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"), \Request::input("sortType", "asc"));

        // paginate results
        if ($resPerPage = \Request::input("perPage"))
            return $query->paginate($resPerPage);
        return $query->get();
    }

    public static function validationRules($attributes = null)
    {
        $rules = [
            'name' => 'required|string|max:255|unique:parking_lots',
            'title' => 'required|string|max:255',
            'extension' => 'required|string|max:255',
        ];

        // no list is provided
        if (!$attributes)
            return $rules;

        // a single attribute is provided
        if (!is_array($attributes))
            return [$attributes => $rules[$attributes]];

        // a list of attributes is provided
        $newRules = [];
        foreach ($attributes as $attr)
            $newRules[$attr] = $rules[$attr];
        return $newRules;
    }

    public static function getFromName($name)
    {
        return static::whereName($name)->first();
    }
}
