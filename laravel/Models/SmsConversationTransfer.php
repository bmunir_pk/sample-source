<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SmsConversationTransfer *
 * @property  integer $id
 * @property  integer $conversation_id
 * @property  integer $transferer_role
 * @property  integer $transferer_username
 * @property  integer $transferred_from
 * @property  integer $transferred_to
 * @property  integer $transferred_at
 * @property  integer $created_at
 * @property  integer $updated_at
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversationTransfer whereId($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversationTransfer whereConversationId($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversationTransfer whereTransfererRole($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversationTransfer whereTransfererUsername($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversationTransfer whereTransferredFrom($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversationTransfer whereTransferredTo($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversationTransfer whereTransferredAt($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversationTransfer whereCreatedAt($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversationTransfer whereUpdatedAt($value)
 */
class SmsConversationTransfer extends Model
{
    public $guarded = ["id", "created_at", "updated_at"];

    public static function findRequested()
    {
        $query = SmsConversationTransfer::query();

        // search results based on user input
        \Request::has('id') and $query->where('id',\Request::input('id'));
        \Request::has('conversation_id') and $query->where('conversation_id',\Request::input('conversation_id'));
        \Request::has('transferer_role') and $query->where('transferer_role','like','%'.\Request::input('transferer_role').'%');
        \Request::has('transferer_username') and $query->where('transferer_username','like','%'.\Request::input('transferer_username').'%');
        \Request::has('transferred_from') and $query->where('transferred_from','like','%'.\Request::input('transferred_from').'%');
        \Request::has('transferred_to') and $query->where('transferred_to','like','%'.\Request::input('transferred_to').'%');
        \Request::has('transferred_at') and $query->where('transferred_at',\Request::input('transferred_at'));
        \Request::has('created_at') and $query->where('created_at',\Request::input('created_at'));
        \Request::has('updated_at') and $query->where('updated_at',\Request::input('updated_at'));
        
        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"), \Request::input("sortType", "asc"));

        // paginate results
        if ($resPerPage = \Request::input("perPage"))
            return $query->paginate($resPerPage);
        return $query->get();
    }

    public static function validationRules($attributes = null)
    {
        $rules = [
            'conversation_id' => 'required|integer',
            'transferer_role' => 'required|string|max:255',
            'transferer_username' => 'required|string|max:255',
            'transferred_from' => 'required|string|max:255',
            'transferred_to' => 'required|string|max:255',
            'transferred_at' => 'required|date',
        ];

        // no list is provided
        if (!$attributes)
            return $rules;

        // a single attribute is provided
        if (!is_array($attributes))
            return [$attributes => $rules[$attributes]];

        // a list of attributes is provided
        $newRules = [];
        foreach ($attributes as $attr)
            $newRules[$attr] = $rules[$attr];
        return $newRules;
    }

}
