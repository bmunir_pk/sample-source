<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AppSetting
 *
 * @property integer $id
 * @property string $config
 * @property string $value
 * @property string $comments
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AppSetting whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AppSetting whereConfig($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AppSetting whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AppSetting whereComments($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AppSetting whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AppSetting whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $category
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AppSetting whereCategory($value)
 * @property boolean $enabled
 * @property boolean $show_enabled_switch
 * @method static \Illuminate\Database\Query\Builder|AppSetting whereEnabled($value)
 * @method static \Illuminate\Database\Query\Builder|AppSetting whereShowEnabledSwitch($value)
 */
class AppSetting extends Model
{
    const CACHE_KEY = 'app-settings';
    public static $editableFields = [
        'enabled',
        'value',
    ];

    public function validationRules()
    {
        $rules = [
            'enabled' => 'bool',
            'value' => 'required',
        ];

        switch ($this->config) {
            case 'No. of concurrent active admins':
                $rules = ['value' => 'required|integer|min:1'];
                break;
            case 'No. of concurrent active agents':
                $rules = ['value' => 'required|integer|min:1'];
                break;
            case 'Call control status':
            case 'Chat report status':
            case 'Flash report status':
            case 'Call Disposition Status':
                $rules = ['value' => 'required|in:enabled,disabled'];
                break;
            case 'Flash report frequency':
                $rules = ['value' => 'required|integer|between:1,12'];
                break;
            case 'Calls to secondary site':
                $rules = ['value' => 'required|integer|between:1,100'];
                break;
        }

        return $rules;
    }

    public static function getAll()
    {
        if(!$settings = app('redis')->get(static::CACHE_KEY))
            return static::all()->toArray();
        return json_decode($settings, true);
    }

    public static function getConfig($key)
    {
        if ($settings = static::getAll()) {
            return collect($settings)->where('config', $key)->first();
        }
        return "";
    }

    public static function getValueFor($key)
    {
        $config = self::getConfig($key);
        if ($config) return array_get($config, 'value');
        return "";
    }

    public static function saveToCache()
    {
        app('redis')->set(static::CACHE_KEY, json_encode(static::all()->toArray()));
    }
}
