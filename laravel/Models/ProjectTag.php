<?php

namespace App\Models;
use App\Traits\InputSanitizer;
use Illuminate\Database\Eloquent\Model;

class ProjectTag extends Model
{
    //
    use InputSanitizer;
    protected $fillable = ['project_id', 'tag_id'];
    public $timestamps = false;

    public function tagTitle () {
        return $this->hasOne(Tag::class, 'id', 'tag_id');
    }
}
