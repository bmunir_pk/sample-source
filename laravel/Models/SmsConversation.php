<?php

namespace App\Models;

use App\Helpers\SocialTextHelper;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SmsConversation *
 * @property  integer $id
 * @property  integer $queue
 * @property  integer $agent_id
 * @property  integer $customer_number
 * @property  integer $started_at
 * @property  integer $status
 * @property  integer $display_status
 * @property  integer $first_replied_at
 * @property  integer $completed_at
 * @property  integer $customer_rating
 * @property  integer $disposition_id
 * @property  integer $dispositioned_at
 * @property  boolean $needs_followup
 * @property  boolean $followup_completed
 * @property  integer $followup_disposition_id
 * @property  string $followup_phone_type
 * @property  string $followup_phone
 * @property  integer $disposition_notes
 * @property  integer $created_at
 * @property  integer $updated_at
 * @property  integer $queue_id
 * @property  integer $welcome_msg_sent
 * @property  integer $needs_disposition
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversation whereId($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversation whereQueue($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversation whereAgentId($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversation whereCustomerNumber($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversation whereStartedAt($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversation whereStatus($value)
 * @method  static \Illuminate\Database\Query\Builder|SmsConversation whereDisplayStatus($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversation whereFirstRepliedAt($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversation whereCompletedAt($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversation whereCustomerRating($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversation whereDispositionId($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversation whereDispositionedAt($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversation whereNeedsFollowup($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversation whereFollowupCompleted($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversation whereFollowupDispositionId($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversation whereFollowupPhoneType($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversation whereFollowupPhone($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversation whereDispositionNotes($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversation whereCreatedAt($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversation whereUpdatedAt($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversation whereQueueId($value)
 * @method  static \Illuminate\Database\Query\Builder|\App\Models\SmsConversation whereWelcomeMsgSent($value)
 * @method  static \Illuminate\Database\Query\Builder|SmsConversation whereNeedsDisposition($value)
 * @property-read \App\Models\Campaign $campaign
 */
class SmsConversation extends Model
{
    public $guarded = ["id", "created_at", "updated_at"];
    public $appends = ['startedTS', 'completedTS', 'dispoTS', 'updatedTS'];

    public function getUpdatedTSAttribute()
    {
        return time() - strtotime($this->updated_at);
    }

    public function getStartedTSAttribute()
    {
        return time() - strtotime($this->started_at);
    }

    public function getCompletedTSAttribute()
    {
        if(!$this->completed_at) return null;
        return time() - strtotime($this->completed_at);
    }

    public function getDispoTSAttribute()
    {
        if(!$this->dispositioned_at) return null;
        return time() - strtotime($this->dispositioned_at);
    }

    public function getMessageConfig($key)
    {
        $config = AppSetting::getConfig($key);
        if (!$config || !array_get($config, 'enabled')) return "";
        return array_get($config, 'value');
    }

    // Is the message sent whenever a conversation is dispositioned
    public function sendRatingMessage()
    {
        $goodByeMessage = $this->getMessageConfig('Rating Message');
        if (DontCallNumberList::findNumber($this->customer_number)) {
            return;
        }
        // check if this is a campaign conversation
        if ($this->campaign_id > 0) {
            $campaign = Campaign::find($this->campaign_id);
            $number = CampaignNumber::where('campaign_id', $this->campaign_id)
                ->join('phone_numbers', 'phone_numbers.id', '=', 'campaign_numbers.number_id')
                ->select('campaign_numbers.id as id', 'phone_numbers.number');
            $number = numberQuery($number, 'number', $this->customer_number)->first();
            // check if sendFeedback is disabled then return
            if ($campaign->feedback_message_text && $number) {
                $goodByeMessage = SmsMessage::textTransformation($campaign->feedback_message_text, $this->customer_number, $this->queue, $number->id);
            }
        }

        //If rating message is disabled then only Goodbye Message will be sent right after the disposition.
        if (!$goodByeMessage) {
            $this->sendClosingMessage();
            return;
        }

        if ($this->customer_rating) return;

        $messagePayload =[
            'to' => $this->customer_number,
            'from' => $this->queue,
            'text' => $goodByeMessage,
        ];

        if($this->queue_icon == 'did' || empty($this->queue_icon)){
            app('smsApi')->createMessage($messagePayload);
        } else if ($this->queue_icon == 'whatsapp') {
            $message = SocialTextHelper::sendMessageToWhatsApp($messagePayload);
        } else if ($this->queue_icon == 'facebook') {
            $message = SocialTextHelper::sendMessageToFB($messagePayload);
        }

        $this->save();
    }

    // Rating Message will request a rating and if the user sends a rating then the goodbye message is sent.
    public function sendClosingMessage()
    {
        $closingSms = $this->getMessageConfig('Goodbye Message');
        if (!$closingSms) return;
        $closingSms = str_replace('{agent_number}', $this->queue, $closingSms);
        $messagePayload = [
            'to' => $this->customer_number,
            'from' => $this->queue,
            'text' => $closingSms,
        ];

        if($this->queue_icon == 'did' || empty($this->queue_icon)){
            app('smsApi')->createMessage($messagePayload);
        } else if ($this->queue_icon == 'whatsapp') {
            $message = SocialTextHelper::sendMessageToWhatsApp($messagePayload);
        } else if ($this->queue_icon == 'facebook') {
            $message = SocialTextHelper::sendMessageToFB($messagePayload);
        }

        //SmsMessage::createMessage($this->id, $message);

        $this->save();
    }

    //Is the message that gets sent once whenever a user sends a message to the panel for the first time
    public function sendWelcomeMessage()
    {
        $welcomeMessage = $this->getMessageConfig('Company Welcome Message');
        if (!$welcomeMessage || $this->welcome_msg_sent || $this->agent_id > 0) return;
        $messagePayload = [
            'to' => $this->customer_number,
            'from' => $this->queue,
            'text' => $welcomeMessage,
        ];
        if($this->queue_icon == 'did' || empty($this->queue_icon)) {
            $message = app('smsApi')->createMessage($messagePayload);
        } else if ($this->queue_icon == 'whatsapp') {
            //$message['to'] = str_replace('3336109665', '+923336109665', $messagePayload['to']);
            $message = SocialTextHelper::sendMessageToWhatsApp($messagePayload);
        } else if ($this->queue_icon == 'facebook') {
            //$message['to'] = str_replace('3336109665', '+923336109665', $messagePayload['to']);
            $message = SocialTextHelper::sendMessageToFB($messagePayload);
        }
        if (!$message) return;

        SmsMessage::createMessage($this->id, $message);

        $this->welcome_msg_sent = 1;
        $this->save();
    }

    public function sendAfterHourMessage()
    {
        $message = $this->getMessageConfig('After Hours Message');
        if (!$message) return;
        $workingDay = WorkingHour::getCurrentDay();
        if (WorkingHour::isWorkingHours($workingDay)) return;
        $businessHours = array_get($workingDay, 'working_day');
        if (!array_get($workingDay, 'status', 0)) {
            $businessHours .= ' is Off';
        } else {
            $businessHours .= " (" . array_get($workingDay, 'start_time') . "-" . array_get($workingDay, 'end_time') . ")";
        }
        $dynamicKeys = [
            'Company Name' => AppSetting::getValueFor('Company Name'),
            'Hours of Operation' => $businessHours,
        ];
        foreach ($dynamicKeys as $key => $value) {
            $message = str_replace("[{$key}]", $value, $message);
        }
        $message = app('smsApi')->createMessage([
            'to' => $this->customer_number,
            'from' => $this->queue,
            'text' => $message,
        ]);

        SmsMessage::createMessage($this->id, $message);
    }

    public function notifyNewConversation()
    {
        $this->load('messages');
        app('pusher')->trigger("agent-{$this->agent_id}", "new-sms-conversation", $this->toArray());
    }

    public static function findRequested()
    {
        $query = SmsConversation::with(['tones' => function ($query) {
            $query->select([\DB::raw('Count(*) as count'), \DB::raw('AVG(score) as score'), 'tone_name', 'tone_id'])
                ->groupBy('tone_name')
                ->orderBy('count', 'desc')
                ->limit(4);
        }]);
        $query->leftJoin("dispositions", "sms_conversations.disposition_id", "=", "dispositions.id");
        $query->select("sms_conversations.*", "dispositions.name as disposition", "dispositions.category");
        $query->with(array('campaign' => function ($query) {
            $query->select('id', 'title');
        }));

        // search results based on user input
        \Request::has('id') and $query->where('id', \Request::input('id'));
        \Request::has('queue') and $query->where('queue', 'like', '%' . \Request::input('queue') . '%');
        \Request::has('agent_id') and $query->where('agent_id', \Request::input('agent_id'));
        \Request::has('customer_number') and $query->where('customer_number', 'like', '%' . stripNumber(\Request::input('customer_number')) . '%');

        \Request::has('status') and $query->where('sms_conversations.status', 'like', '%' . \Request::input('status') . '%');
        \Request::has('display_status') and $query->where('display_status', \Request::input('display_status'));
        if (\Request::has('conversationStatus')) {
            // conversationStatus comes from the real time page at the moment
            // 0 means we want to load conversations that are NOT completed yet
            // 1 means we want to load conversations that are completed
            $statuses = [['Waiting for agent', 'Ongoing'], ['Completed', 'Dispositioned']];
            $date = date('Y-m-d', strtotime('-1 months'));
            if (\Request::input('conversationStatus')) {
                $params = ['statuses' => $statuses[1], 'date' => $date];
                $query->where(function ($q) use ($params) {
                    $q->whereIn('sms_conversations.status', $params['statuses']);
                    $q->orWhere(function ($q) use ($params) {
                        $q->whereNotIn('sms_conversations.status', $params['statuses']);
                        $q->whereDate('sms_conversations.updated_at', '<=', $params['date']);
                    });
                });
            } else {
                $query->whereIn('sms_conversations.status', $statuses[0]);
                $query->whereDate('sms_conversations.updated_at', '>', $date);
            }
        }

        \Request::has('first_replied_at') and $query->where('first_replied_at', \Request::input('first_replied_at'));
        \Request::has('customer_rating') and $query->where('customer_rating', 'like', '%' . \Request::input('customer_rating') . '%');
        \Request::has('disposition_id') and $query->where('disposition_id', \Request::input('disposition_id'));
        \Request::has('needs_followup') and $query->where('needs_followup', \Request::input('needs_followup'));
        \Request::has('followup_completed') and $query->where('followup_completed', \Request::input('followup_completed'));
        \Request::has('followup_disposition_id') and $query->where('followup_disposition_id', \Request::input('followup_disposition_id'));
        \Request::has('followup_phone_type') and $query->where('followup_phone_type', \Request::input('followup_phone_type'));
        \Request::has('followup_phone') and $query->where('followup_phone', \Request::input('followup_phone'));
        \Request::has('disposition_notes') and $query->where('disposition_notes', 'like', '%' . \Request::input('disposition_notes') . '%');
        \Request::has('created_at') and $query->where('sms_conversations.created_at', \Request::input('created_at'));
        \Request::has('queue_id') and $query->where('queue_id', 'like', '%' . \Request::input('queue_id') . '%');
        \Request::has('welcome_msg_sent') and $query->where('welcome_msg_sent', \Request::input('welcome_msg_sent'));
        \Request::has('needs_disposition') and $query->where('needs_disposition', \Request::input('needs_disposition'));
        \Request::has('username') and $query->where('username', 'like', '%' . \Request::input('username') . '%');
        //Disposition
        \Request::has('disposition') and $query->where(function ($query) {
            $query->where('dispositions.name', 'like', '%' . \Request::input('disposition') . '%');
            $query->orWhere('dispositions.category', 'like', '%' . \Request::input('disposition') . '%');
        });
        //Widget
        \Request::has('widget') and $query->where(function ($query) {
            if (\Request::input('widget') == 'SMS') {
                $query->whereNull('queue')->where('sms_conversations.status', '=', 'Ongoing');
            } else if (\Request::input('widget') == 'CC SMS') {
                $query->whereNotNull('queue');
            }
        });

        //Start Date Range filters
        $dateColumns = [
            'completed_at',
            'dispositioned_at',
            'started_at',
            'updated_at',
        ];
        foreach ($dateColumns as $column) {
            if (\Request::has($column)) {
                $query = dateFilterQuery($query, \Request::get($column), $column, 'sms_conversations');
            }
        }
        //End Date Range filters

        $admin = \Auth::guard('admin')->user();
        if (!$admin->isSuperAdmin()) {
            $query->where(function ($query) use ($admin) {
                $query->where(function ($query) use ($admin) {
                    $query->whereNotNull('queue_id')->whereIn('queue_id', $admin->smsQueuesAllowed());
                })->orWhere(function ($query) use ($admin) {
                    $agentIds = FreepbxUser::whereIn('location_id', $admin->locationsAllowed())->select('id')->pluck('id')->toArray();
                    $query->orWhereNull('queue_id')->whereIn('agent_id', $agentIds);
                });
            });
        }

        //Don't load campaign conversation customer didn't reply
        $query->where(function ($query) {
            $query->where('campaign_id', '=', 0);
            $query->orWhere(function ($query) {
                $query->where('campaign_id', '!=', 0);
                $query->where('num_messages', '>', 1);
            });
        });

        // sort results
        $sort = \Request::input("sort");
        $sortType = \Request::input("sortType", "asc");
        if ($sort == 'needs_disposition') {
            $sortType = $sortType == 'asc' ? 'desc' : 'asc'; //sort cc sms as asc
        }
        \Request::input("sort") and $query->orderBy($sort, $sortType);

        // paginate results
        if ($resPerPage = \Request::input("perPage")) {
            return $query->paginate($resPerPage);
        }
        return $query->get();
    }

    public static function validationRules($attributes = null)
    {
        $rules = [
            'queue' => 'required|string|max:255',
            'agent_id' => 'integer',
            'customer_number' => 'required|string|max:255',
            'started_at' => 'required|date',
            'status' => 'required|in:Waiting for agent,Ongoing,Completed,Dispositioned',
            'display_status' => 'string',
            'first_replied_at' => 'date',
            'completed_at' => 'date',
            'customer_rating' => 'string|max:255',
            'disposition_id' => 'integer',
            'dispositioned_at' => 'date',
            'needs_followup' => 'bool',
            'followup_completed' => 'bool',
            'followup_disposition_id' => 'integer',
            'followup_phone_type' => 'required_if:needs_followup,1|in:Cell Phone,Home,Office',
            'followup_phone' => 'required_if:needs_followup,1',
            'disposition_notes' => 'string',
            'queue_id' => 'required|integer',
            'welcome_msg_sent' => 'required',
            'needs_disposition' => 'bool',
        ];

        // no list is provided
        if (!$attributes)
            return $rules;

        // a single attribute is provided
        if (!is_array($attributes))
            return [$attributes => $rules[$attributes]];

        // a list of attributes is provided
        $newRules = [];
        foreach ($attributes as $attr)
            $newRules[$attr] = $rules[$attr];
        return $newRules;
    }

    public function messages()
    {
        return $this->hasMany(SmsMessage::class)->select('sms_messages.*', \DB::raw('Date(date_time) as date'));
    }

    public function disposition()
    {
        return $this->belongsTo(Disposition::class);
    }

    public function agent()
    {
        return $this->belongsTo(FreepbxUser::class, 'agent_id');
    }

    public function tones()
    {
        return $this->hasManyThrough(SmsMessageTone::class, SmsMessage::class);
    }

    public static function getFollowUpWork()
    {
        return SmsConversation::where([
            ['needs_followup', 1],
            ['followup_completed', 0],
            ['agent_id', \Auth::id()],
        ])->with('disposition')->get();
    }

    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    public function statusByLastMessage()
    {
        $lastMessage = SmsMessage::whereSmsConversationId($this->id)
            ->orderBy('id', 'desc')
            ->first();
        if (!$lastMessage) return 'Ongoing';
        return $lastMessage->direction == 'in' ? 'Received' : 'Sent';
    }
}
