<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SipContact *
 *
 * @property integer $id
 * @property integer $name
 * @property string $company
 * @property string $notes
 * @property integer $group_id
 * @property integer $created_at
 * @property integer $updated_at
 * @method static \Illuminate\Database\Query\Builder|SipContact whereId($value)
 * @method static \Illuminate\Database\Query\Builder|SipContact whereName($value)
 * @method static \Illuminate\Database\Query\Builder|SipContact whereCompany($value)
 * @method static \Illuminate\Database\Query\Builder|SipContact whereNotes($value)
 * @method static \Illuminate\Database\Query\Builder|SipContact whereGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|SipContact whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|SipContact whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SipContact extends Model
{

    public $guarded = ["id", "created_at", "updated_at"];
    public $appends = ['isFavorite'];

    /**
     * @return bool
     */
    public function getIsFavoriteAttribute()
    {
        if ($this->favorite())
            return true;
        return false;
    }

    public static function findRequested()
    {
        $query = SipContact::query()->with(['details']);

        // search results based on user input
        \Request::input('id') and $query->where('id', \Request::input('id'));
        \Request::input('name') and $query->where('name', 'like', '%' . \Request::input('name') . '%');
        \Request::input('company') and $query->where('company', 'like', '%' . \Request::input('company') . '%');
        \Request::has('notes') and $query->where('notes', 'like', '%' . \Request::input('notes') . '%');
        \Request::input('group_id') and $query->where('group_id', \Request::input('group_id'));
        \Request::input('created_at') and $query->where('created_at', \Request::input('created_at'));
        \Request::input('updated_at') and $query->where('updated_at', \Request::input('updated_at'));
        \Request::has('groups') and $query->whereIn('group_id', \Request::input('groups'));

        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"), \Request::input("sortType", "asc"));

        // paginate results
        if ($resPerPage = \Request::input("perPage"))
            return $query->paginate($resPerPage);
        return $query->get();
    }

    public function getFavoriteAttribute($value)
    {
        return $value == 1;
    }

    public static function validationRules($attributes = null)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'company' => 'string|max:255',
            'notes' => 'string|max:255',
            'group_id' => 'required|integer',
        ];

        // no list is provided
        if (!$attributes)
            return $rules;

        // a single attribute is provided
        if (!is_array($attributes))
            return [$attributes => $rules[$attributes]];

        // a list of attributes is provided
        $newRules = [];
        foreach ($attributes as $attr)
            $newRules[$attr] = $rules[$attr];
        return $newRules;
    }

    public function details()
    {
        return $this->hasMany(SipContactNumber::class, 'sip_contact_id', 'id');
    }

    public function group()
    {
        return $this->belongsTo(SipContactGroup::class, 'group_id');
    }

    public function favorite()
    {
        return SipContactsFavorite::where([
            ['contact_id', $this->id],
            ['agent_id', \Auth::id()],
        ])->first();
    }
}
