<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PhoneNumber *
 *
 * @property integer $id
 * @property integer $number
 * @property string $first_name
 * @property string $last_name
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PhoneNumber whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PhoneNumber whereNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PhoneNumber whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PhoneNumber whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PhoneNumber whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PhoneNumber whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PhoneNumber whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PhoneNumber extends Model {

    public $guarded = ["id","created_at","updated_at"];
    public static $uploadFields = [
        'first_name' => [
            'label' => 'first name',
            'index' => false
        ],
        'last_name' => [
            'label' => 'last name',
            'index' => false
        ],
        'number' => [
            'label' => 'numbers',
            'index' => false
        ],
    ];

    public static function findRequested()
    {
        $query = PhoneNumber::query();

        // search results based on user input
        \Request::input('id') and $query->where('id',\Request::input('id'));
        \Request::input('number') and $query->where('number','like','%'.\Request::input('number').'%');
        \Request::input('first_name') and $query->where('first_name','like','%'.\Request::input('first_name').'%');
        \Request::input('last_name') and $query->where('last_name','like','%'.\Request::input('last_name').'%');
        \Request::input('status') and $query->where('status',\Request::input('status'));
        \Request::input('created_at') and $query->where('created_at',\Request::input('created_at'));
        \Request::input('updated_at') and $query->where('updated_at',\Request::input('updated_at'));

        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"),\Request::input("sortType","asc"));

        // paginate results
        if( $resPerPage = \Request::input("perPage") )
            return $query->paginate($resPerPage);
        return $query->get();
    }

    public static function validationRules( $attributes = null )
    {
        $rules = [
            'number' => 'required|string|max:255',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'status' => 'required|integer',
        ];

        // no list is provided
        if(!$attributes)
            return $rules;

        // a single attribute is provided
        if(!is_array($attributes))
            return [ $attributes => $rules[$attributes] ];

        // a list of attributes is provided
        $newRules = [];
        foreach ( $attributes as $attr )
            $newRules[$attr] = $rules[$attr];
        return $newRules;
    }

    public static function updateOrCreate($attributes, $values = [])
    {
        $instance = PhoneNumber::firstOrNew($attributes);
        $instance->fill($values)->save();
        return $instance;
    }
}
