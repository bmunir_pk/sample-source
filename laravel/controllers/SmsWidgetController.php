<?php

namespace App\Modules\Front\Controllers;

use App\Helpers\BandwidthHelper;
use App\Helpers\SmsCommandHelper;
use App\Helpers\SocialTextHelper;
use App\Http\Controllers\Controller;
use App\Jobs\HandleIncomingSms;
use App\Models\Campaign;
use App\Models\CampaignNumber;
use App\Models\Disposition;
use App\Models\DispositionCategory;
use App\Models\FreepbxUser;
use App\Models\PhoneNumber;
use App\Models\ScheduledMessage;
use App\Models\SipContact;
use App\Models\SipContactGroup;
use App\Models\SmsConversation;
use App\Models\SmsMessage;
use App\Models\SmsQueue;
use App\RedisModels\Agent;
use App\RedisModels\SmsQueueRedis;
use App\Traits\HasLogFile;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SmsWidgetController extends Controller
{
    use HasLogFile;
    private $logFile = "sms-provisioning";
    private $bwFileSizeLimit = 1500000; //In Bytes

    public function loadDynamicFields()
    {
        return SmsMessage::dynamicFields();
    }

    public function smsWidget()
    {
        return view('front.dashboard.widgets.sms-widget-template', ['type' => 'Regular']);
    }

    public function contactCenterSmsWidget()
    {
        return view('front.dashboard.widgets.sms-widget-template', ['type' => 'ContactCenter']);
    }

    public function loadScheduledMessages()
    {
        return \Auth::user()->scheduledMessages;
    }

    /**
     * Implements the following api:
     * https://doc.acrobits.net/api/client/fetch_messages.html
     * @return array
     */
    public function fetchMessagesForAcrobits()
    {
        $this->log('----------------------- start fetching messages for acrobits -----------------------');
        $this->log(\Request::all());
        $username = \request('username');
        $lastSmsId = \request('last_id');
        $lastSentSmsId = \request('last_sent_id');
        $queueSms = \request('queue_sms') == '1';
        $agent = FreepbxUser::whereUsername($username)->first();
        if (!$agent) {
            return response("Your account (ext: {$username}) has not been created yet. Please contact administrator.", 400);
        }
        $number = $agent->did_for_sms;
        $queues = [];
        $commonQuery = SmsMessage::query();
        if ($queueSms) {
            $conversations = SmsConversation::whereAgentId($agent->id)
                ->where(function ($query) {
                    $query->where('needs_disposition', 1);
                    $query->orWhere(function ($query) {
                        $query->where('needs_disposition', 0);
                        $query->where(function ($query) {
                            $query->where('customer_number', SmsCommandHelper::number());
                            $query->orWhere('customer_number', SmsCommandHelper::numberWithoutCode());
                        });
                    });
                })
                ->whereIn('status', ['Waiting for agent', 'Ongoing'])
                ->select('id', 'queue');
            $queues = $conversations->pluck('queue')->toArray();
            $commonQuery->whereIn('sms_conversation_id', $conversations->pluck('id')->toArray());
        }

        // get read and unread sms
        $query = clone $commonQuery;
        $query->select(\DB::raw("id as sms_id, date_time, date_time as sending_date, `from` as sender, text as sms_text, `read`"));
        if (!$queueSms) {
            $query->where(function ($query) use ($number) {
                $query->where('to', $number)->orWhere('to', str_replace('+1', '', $number));
            });
        } else {
            $query->where(function ($query) use ($queues) {
                $query->whereIn('to', $queues);
                $query->orWhere('from', SmsCommandHelper::number());
            });
        }
        if ($lastSmsId) $query->where('id', '>', $lastSmsId);

        $inMessages = $query->get()->groupBy("read")->toArray();

        // get sent sms
        $query = clone $commonQuery;
        $query->select(\DB::raw("id as sms_id, date_time, date_time as sending_date, `to` as recipient, text as sms_text"));
        if (!$queueSms) {
            $query->where(function ($query) use ($number) {
                $query->where('from', $number)->orWhere('from', str_replace('+1', '', $number));
            });
        } else {
            $query->where(function ($query) use ($queues) {
                $query->whereIn('from', $queues);
                $query->orWhere('to', SmsCommandHelper::number());
            });
        }
        if ($lastSmsId) $query->where('id', '>', $lastSentSmsId);

        $outMessages = $query->get()->toArray();

        // prepare response in expected format
        $response = [
            'date' => date(DATE_RFC3339),
            'unread_smss' => array_get($inMessages, ""),
            'read_smss' => array_get($inMessages, 1),
            'sent_smss' => $outMessages,
        ];

        $this->log("Response to api: " . toStr($response));
        $this->log('----------------------- end fetching messages for acrobits -----------------------');
        return $response;
    }

    /**
     * This action is called when sending message through acrobits softphone
     * @return array
     */
    public function sendMessageForAcrobits()
    {
        $this->log('----------------------- start sending message for acrobits -----------------------');
        $this->log(\Request::all());
        $username = request('username');
        $queueSms = \request('queue_sms') == '1';
        $agent = FreepbxUser::whereUsername($username)->first();
        if (!$agent) {
            $this->log("Your account (ext: {$username}) has not been created yet. Please contact administrator.");
            return response("Your account (ext: {$username}) has not been created yet. Please contact administrator.", 400);
        }
        $agent = $agent->redisStatus();

        $from = $agent->did_for_sms;
        $to = request('to');
        $text = request('body');
        $isCommandNumber = SmsCommandHelper::isHelpNumber($to);

        if ($queueSms && !$isCommandNumber) {
            $conversation = SmsConversation::where('agent_id', $agent->id);
            $conversation = numberQuery($conversation, 'customer_number', $to);
            $conversation = $conversation->whereNotNull('queue')
                ->where('needs_disposition', 1)
                ->whereIn('status', ['Waiting for agent', 'Ongoing'])
                ->orderBy('id', 'desc')
                ->first(); // order by id desc in case the client had multiple conversations before
            if ($conversation) {
                $this->log("Conversation: {$conversation->id}");
                $dispositionCommand = '#dispo ';
                $from = $conversation->queue;
                $this->log("From number set from conversation queue: {$from}");
                //Handle conversation actions
                switch ($text) {
                    case starts_with(strtolower($text), $dispositionCommand):
                        $dispositionId = trim(str_replace($dispositionCommand, "", $text));
                        $disposition = Disposition::where([
                            ['id', '=', $dispositionId],
                            ['status', '=', 1],
                            ['for_sms', '=', 1],
                        ])->first();
                        if ($disposition) {
                            $conversation->status = 'Dispositioned';
                            $conversation->disposition_id = $dispositionId;
                            $conversation->completed_at = $conversation->dispositioned_at = date('Y-m-d H:i:s');
                            $conversation->save();
                            // request a feedback
                            $conversation->sendGoodByeMessage();
                            $agent->removeFromSet(Agent::$smsConversationsSet, $conversation->id);
                            return "Dispositioned";
                        } else {
                            return response('Invalid disposition id', 422);
                        }
                        break;
                }
            } else {
                $this->log("creating new sms");
                //Send message from queue for queue user
                $agent = Agent::find(request('username'));
                if (!$agent || !count($agent->smsQueues)) {
                    return response("You are not logged into queue", 422);
                }
                $this->log("Setting sms queue: " . toStr($agent->smsQueues[0]));
                $from = $agent->smsQueues[0]->number;
                $this->log("From number set from logged queues: {$from}");
            }
        }

        if (!$from) {
            $this->log("No DID has been set up for your account (ext: {$username}) yet. Please contact administrator.");
            $this->log($agent);
            return response("No DID has been set up for your account (ext: {$username}) yet. Please contact administrator.", 400);
        }

        if (!$to) {
            return response("Please specify a destination number", 400);
        }

        if (!$text) {
            return response("Please enter some text to send.", 400);
        }

        if ($isCommandNumber) {
            return SmsCommandHelper::handleMessage($agent, $text);
        }

        $this->log("Sending from {$from} to {$to}");
        $response = SmsMessage::sendMessage($agent, $from, $to, $text);
        $message = array_get($response, 'message');

        app('pusher')->trigger('messages-' . $agent->id, 'new', $message);
        $this->log('----------------------- end sending message for acrobits -----------------------');
        return ['sms_id' => array_get($message, 'id')];
    }

    public function loadAgentQueues()
    {
        $queues = SmsQueueRedis::all();
        $agentQueues = \Auth::user()->redisStatus()->smsQueues;
        $result = [];
        foreach ($queues as $queue) {
            if (in_array($queue->id, $agentQueues)) {
                $result[] = $queue->toArray();
            }
        }
        return $result;
    }

    public function loadRecentContacts()
    {
        return SmsConversation::select([
            \DB::raw("REPLACE(customer_number,'+1','') as number"),
            'customer_number',
            \DB::raw("MAX(updated_at) as updated_at"), //Pickup latest time with group by
        ])
            ->where('agent_id', \Auth::user()->id)
            ->groupBy('number') //load unique numbers
            ->orderBy('updated_at', 'desc')
            ->limit(8)
            ->get();
    }

    public function disposition(Request $request)
    {
        $rules = [
            'disposition_id' => 'required|integer',
            'category' => 'required',
            'needs_followup' => 'sometimes',
            'followup_phone' => 'required_if:needs_followup,1',
            'followup_phone_type' => 'required_if:needs_followup,1|in:Cell Phone,Home,Office',
        ];
        $this->validate($request, $rules);

        $conversation = SmsConversation::find(request('id'));
        /* @var $conversation SmsConversation */
        if (!$conversation) {
            abort(400, 'Unknown conversation.');
        }

        $conversation->status = 'Dispositioned';
        $conversation->disposition_id = $request->disposition_id;
        $conversation->dispositioned_at = date('Y-m-d H:i:s');
        if ($needs_followup = $request->get('needs_followup')) {
            $conversation->needs_followup = $needs_followup;
            $conversation->followup_phone_type = $request->get('followup_phone_type');
            $conversation->followup_phone = $request->get('followup_phone');
            $conversation->disposition_notes = $request->get('disposition_notes');
        }
        $conversation->save();
        // request a feedback or goodbye message
        $conversation->sendRatingMessage();
        if ($agent = \Auth::user()->redisStatus()) {
            $agent->removeFromSet(Agent::$smsConversationsSet, $conversation->id);
        }
    }

    public function loadDispositions()
    {
        return DispositionCategory::loadDispositionsFor('for_sms');
    }

    public function markAsCompleted()
    {
        return $this->markConversationAsCompleted(request('conversation'));
    }

    private function markConversationAsCompleted($conversationId)
    {
        $conversation = SmsConversation::find($conversationId);
        /* @var $conversation SmsConversation */
        if (!$conversation) {
            abort(400, 'Unknown conversation.');
        }

        // mark as completed
        $conversation->status = "Completed";
        $conversation->completed_at = date('Y-m-d H:i:s');
        $conversation->save();

        return "The conversation has been marked as Completed.";
    }

    /**
     * This action is called when sending message in an already ongoing conversation
     * conversation id is passed in the request so we know exactly where to save the message
     * @return SmsMessage
     */
    public function sendMessage()
    {
        $conversation = SmsConversation::find(request('conversation'));

        if (!$conversation) {
            abort(400, 'Unknown conversation.');
        }

        $from = $conversation->needs_disposition ? $conversation->queue : \Auth::user()->did_for_sms;
        //$from = stripNumber($from);
        //$to = stripNumber($conversation->customer_number);
        $to = $conversation->customer_number;
        $queueCheck = numberQuery(SmsQueue::query(), 'number', $from)->first();
        $queueCheck = $queueCheck ? $queueCheck : SocialTextHelper::getFBQueue($from);
        if (!$from) {
            abort(400, "You are not logged in to any queues and no DID is set up for you.");
        }
        $text = SmsMessage::textTransformation(request('text'), $conversation->customer_number, $from);
        if (strlen(stripNumber($conversation->customer_number)) != 10 && ($queueCheck && $queueCheck->type == 'DID')) {
            abort(400, "Invalid number: {$conversation->customer_number}");
        }
        if (!$text) {
            abort(400, "Please enter some text to send.");
        }
        $message = [
            'to' => $to,
            'from' => $from,
            'text' => $text,
        ];

        $attachments = [];
        if (request()->hasFile('attachments')) {
            $size = strlen($text);
            foreach (request()->file('attachments') as $attachment) {
                $size += $attachment->getSize();
            }
            if ($size > $this->bwFileSizeLimit) {
                abort(400, "The size of all media files combined cannot be greater than {$this->bwFileSizeLimit} bytes.");
            } else {
                $attachments = $this->uploadAttachments();
                $message['media'] = collect($attachments)->pluck('path')->toArray();
            }
        }

        $message['to'] = str_replace('3336109665', '+923336109665', $message['to']);
        if($queueCheck && $queueCheck->type == 'Whatsapp') {
            $message = SocialTextHelper::sendMessageToWhatsApp($message);
        }
        else if($queueCheck && $queueCheck->type == 'Facebook') {
            $message = SocialTextHelper::sendMessageToFB($message);
        }
        else {
            $message = app('smsApi')->createMessage($message);
        }
        //$message = app('smsApi')->createMessage($message);

        $smsMessage = SmsMessage::createMessage($conversation->id, array_merge($message, ['read' => 1]));
        if (count($attachments)) {
            $smsMessage->attachments()->createMany($attachments);
            $smsMessage->load('attachments');
        }
        return $smsMessage;
    }

    public function markAsRead()
    {
        return SmsMessage::whereSmsConversationId(request('conversation'))->update(['read' => 1]);
    }

    /**
     * This action is called when sending a NEW message from the widget
     * @return array|string
     */
    public function sendOutboundMessage()
    {
        $agent = \Auth::user();
        //$from = stripNumber(request('from'));
        //$to = stripNumber(request('to'));
        $from = request('from');
        $to = request('to');
        $text = request('text');
        $text = SmsMessage::textTransformation($text, $to, $from);
        $queueCheck = numberQuery(SmsQueue::query(), 'number', $from)->first();
        $queueCheck = $queueCheck ? $queueCheck : SocialTextHelper::getFBQueue($from);
        if (strlen(stripNumber($to)) != 10 && ($queueCheck && $queueCheck->type == 'DID')) {
            abort(400, "Invalid number: {$to}");
        }
        $attachments = [];
        if (request()->hasFile('attachments')) {
            $size = strlen($text);
            foreach (request()->file('attachments') as $attachment) {
                $size += $attachment->getSize();
            }
            if ($size > $this->bwFileSizeLimit) {
                abort(400, "The size of all media files combined cannot be greater than {$this->bwFileSizeLimit} bytes.");
            } else {
                $attachments = $this->uploadAttachments();
            }
        }

        // handle scheduled sms
        if (request('is_scheduled_sms')) {
            $date = Carbon::parse(request('scheduled_on'));
            $date->setTimezone(config('app.timezone'));
            $message = ScheduledMessage::create([
                'from' => $from,
                'to' => $to,
                'text' => $text,
                'agent_id' => $agent->id,
                'scheduled_on' => (string)$date,
            ]);
            if (count($attachments)) {
                $message->attachments()->createMany($attachments);
                $message->load('attachments');
            }
            $message->notifyUpdated();
            return $message;
        }

        return SmsMessage::sendMessage($agent, $from, $to, $text, $attachments);
    }

    private function uploadAttachments()
    {
        $data = [];
        $bandwidth = new BandwidthHelper();
        foreach (request()->file('attachments') as $attachment) {
            $path = $bandwidth->upload($attachment);
            if ($path) {
                $data[] = [
                    'file_name' => $attachment->getClientOriginalName(),
                    'file_ext' => $attachment->getClientOriginalExtension(),
                    'file_size' => $attachment->getSize(),
                    'path' => $path,
                    'source' => 'Bandwidth',
                ];
            } else {
                abort(400, "Error uploading media " . $attachment->getClientOriginalName());
            }
        }
        return $data;
    }

    /**
     * This method will be called every time an sms is received.
     * Example data received:
     * array (
     * 'messageId' => 'm-zhfy5q5nm67nzfaqy5lsdhy',
     * 'from' => '+13176897120',
     * 'eventType' => 'sms',
     * 'text' => 'Hey there, this is a test',
     * 'time' => '2018-02-12T07:12:17Z',
     * 'to' => '+13176897120',
     * 'state' => 'received',
     * 'messageUri' => 'https://api.catapult.inetwork.com/v1/users/u-pliay7kvrar52dvcjeymcxa/messages/m-zhfy5q5nm67nzfaqy5lsdhy',
     * 'applicationId' => 'a-il423ldddjblko6tssouclq',
     * 'direction' => 'in',
     * )
     */
    public function handleSmsReceived()
    {
        // log message received
        $content = var_export(request()->all(), true);
        logContent($content, 'sms-activity-' . date('Y-m-d-H') . '.log');
        $content= $this->formatResponse(request()->all());
        // ignore if its an outgoing message
        if (!in_array($content['eventType'], ['sms', 'mms']) || $content['direction'] != 'in') return;

        dispatch(new HandleIncomingSms($content));
    }

    public function formatResponse($data) {
        if(empty($data['SmsMessageSid'])) {
            return $data;
        }

        $temp=[];
        $mapKeys = [
            'From' => 'from',
            'To'=> 'to',
            'SmsMessageSid'=>'messageId',
            'NumSegments' => 'segmentCount',
            'SmsStatus' => 'state',
            'Body' => 'text',
            'MessageSid' => 'messageId',
        ];
        $arrKeys = array_keys($mapKeys);
        foreach ($data as $key=>$val) {
            if(!in_array($key, $arrKeys)) continue;
            if($key == 'From' || $key == 'To') {
                $val = str_replace("whatsapp:", '', $val);
                $val = str_replace("messenger:", '', $val);
                $val = str_replace("facebook:", '', $val);
            }
            $temp[$mapKeys[$key]] = $val;
        }
        $temp['direction'] = 'in';
        $temp['messageUri'] = 'n/a';
        $temp['eventType'] = 'sms';

        logContent($temp, 'sms-activity-' . date('Y-m-d-H') . '.log');
        return $temp;
    }

    private function getCustomerCampaigns($customer_number)
    {
        $phoneNumberIds = numberQuery(PhoneNumber::query(), 'number', $customer_number)
            ->select('id')
            ->pluck('id');
        if (!count($phoneNumberIds)) [];
        $campaignIds = CampaignNumber::whereIn('number_id', $phoneNumberIds)
            ->select('campaign_id')
            ->groupBy('campaign_id')//load unique numbers
            ->orderBy('updated_at', 'desc')
            ->limit(10)
            ->pluck('campaign_id');
        if (!count($campaignIds)) [];
        return Campaign::whereIn('id', $campaignIds)
            ->orderBy('id', 'desc')
            ->get();
    }

    public function load()
    {
        $needsDispo = request('showQueues') == 'true' ? 1 : 0;
        $smsConversations = \Auth::user()->smsConversations()->with(
            array('messages' => function ($query) {
                $query->with(['attachments', 'tones']);
            }))
            ->where('needs_disposition', $needsDispo)
            ->where('sms_conversations.status', '=' ,'Ongoing')
            ->where('sms_conversations.updated_at', '>', Carbon::now()->subMonth(1));

        $smsConversations = $smsConversations->get()->toArray();
        if ($needsDispo) {
            $recentInteractions = \Auth::user()->smsConversations()->with('messages.attachments')
                ->where('needs_disposition', $needsDispo)
                ->where('sms_conversations.status', '!=', 'Ongoing')
                ->orderBy('sms_conversations.updated_at', 'desc')
                ->limit(5)
                ->get();
            if ($recentInteractions) {
                $smsConversations = array_merge($smsConversations, $recentInteractions->toArray());
            }
        }

        foreach ($smsConversations as $key => $smsConversation) {
            $smsConversations[$key]['messages'] = collect($smsConversation['messages'])->groupBy('date');
            if ($needsDispo) {
                $smsConversations[$key]['campaigns'] = $this->getCustomerCampaigns($smsConversation['customer_number']);
            }
        }
        return $smsConversations;
    }

    public function loadArchive()
    {
        $needsDispo = request('showQueues') == 'true' ? 1 : 0;
        $smsConversations = \Auth::user()->smsConversations()->with(
            array('messages' => function ($query) {
                $query->with(['attachments', 'tones']);
            }))
            ->where('needs_disposition', $needsDispo);
        $smsConversations->where('sms_conversations.updated_at', '>', Carbon::now()->subMonth(request('months', 7)));
        $smsConversations = $smsConversations->get()->toArray();

        foreach ($smsConversations as $key => $smsConversation) {
            $smsConversations[$key]['messages'] = collect($smsConversation['messages'])->groupBy('date');
            if ($needsDispo) {
                $smsConversations[$key]['campaigns'] = $this->getCustomerCampaigns($smsConversation['customer_number']);
            }
        }
        $smsConversations = collect($smsConversations)->groupBy(function ($val) {
            return Carbon::parse($val['updated_at'])->format('Y-m');
        });
        return $smsConversations;
    }

    public function updateQueues()
    {
        $agent = \Auth::user()->redisStatus();
        $nodes = request('nodes');
        $agentDataForQSummary = [
            'username' => $agent->username,
            'conversations' => $agent->smsConversationsIds,
        ];
        foreach ($nodes as $node) {
            $redisQueue = SmsQueueRedis::find($node['id']);
            if (!$redisQueue) {
                abort(400, 'Queue not found.');
            }
            if (array_get($node, 'checked')) {
                $redisQueue->addToSet(SmsQueueRedis::$membersSet, $agent->username);
                // notification for sms-queue-summary
                if (!in_array($node['id'], $agent->smsQueuesIds)) {
                    app('pusher')->trigger('sms-queues', 'agent-added', ['queueId' => $node['id'], 'agent' => $agentDataForQSummary]);
                }
                $agent->addToSet(Agent::$smsQueuesSet, $node['id']);
            } else {
                $redisQueue->removeFromSet(SmsQueueRedis::$membersSet, $agent->username);
                // notification for sms-queue-summary
                if (in_array($node['id'], $agent->smsQueuesIds)) {
                    app('pusher')->trigger('sms-queues', 'agent-removed', ['queueId' => $node['id'], 'agent' => $agentDataForQSummary]);
                }
                $agent->removeFromSet(Agent::$smsQueuesSet, $node['id']);
            }
        }

        Agent::notifyStatusChanged($agent->toArray());

        return "Queues updated";
    }

    public function loadQueues()
    {
        $smsQueues = SmsQueueRedis::all();
        $rootNode = [
            'id' => '-1',
            'label' => 'All Queues',
            'opened' => true,
            'children' => [],
        ];

        foreach ($smsQueues as $smsQueue) {
            $node = $smsQueue->toArray();
            $number = $node['type'] != 'did' ? $node['number'] : formatPhoneNumber($node['number']);
            $node['label'] = $node['name'] . " (" . $number . ")";
            $node['checked'] = in_array(\Auth::user()->username, $smsQueue->members);
            $rootNode['children'][] = $node;
        }

        return [$rootNode];
    }
}