<?php

namespace App\Http\Controllers;

use App\Models\ContractedHour;
use App\Models\Project;
use App\Models\ProjectNote;
use App\Models\ProjectTag;
use App\Models\WorkOrder;
use Illuminate\Http\Request;
use App\Models\Projecthistory;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class ProjectController extends Controller
{
    public function create(Request $request)
    {
        $clientId = $request->get('account_id');
        $clientId = $clientId ? $clientId.'-' : '';
        $nextId = DB::table('projects')->max('id') + 1;
        return view('projects.add-project', ['nextId' => $clientId.$nextId, 'projectId'=> $nextId]);
    }

    public function edit(Request $request, $project)
    {
        $project = Project::where('id', $project)
            ->with('hours')
            ->with(['tags' => function ($query) {
                $query->join('tags', 'tags.id', '=', 'tag_id')
                    ->select(\DB::raw('title as text'), 'tags.id', 'project_tags.project_id');
            }])
            ->first();
        $project->date_of_start = date('m/d/Y', strtotime($project->date_of_start));
        $nextId = $project->pid;

        $notes = ProjectNote::where('project_id', $project->id)
            ->with('added_by')
            ->orderBy('id', 'DESC')
            ->get();
        return view('projects.add-project', ['project' => $project, 'nextId'=>$nextId, 'projectId'=> $project->id, 'notes'=> $notes]);
    }

    public function loadProjects()
    {
        $project = new Project();
        return response()->json(['list' => $project->findRequested(), 'total' => $project->findRequested(true)], 200);
    }

    public function loadProjectList()
    {
        $project = new Project();
        return response()->json($project->findRequested(), 200);
    }

    public function loadAllProjects () {
        return response()->json(Project::with('client')->get(), 200);
    }

    public function loadProjectForAssignment () {
        return response()->json(Project::whereNull('client_id')->get(), 200);
    }

    public function saveProject(Request $request)
    {
        if (!\Sentry::getUser()->hasAccess('create_projects') || !\Sentry::getUser()->hasAccess('edit_projects')) {
            return response()->json([
                'message' => "You don't have access to this area"
            ], 403);
        }
        $data = $request->all();
//         print_r($data);
        $project = new Project();
        $rules = Project::$rules;
        if (!empty($data['id'])) {
            $rules['name'] = ['required', Rule::unique('projects')->ignore($request->get('id'))];
        }
        $validator = \Validator::make($data, $rules);
        if ($validator->fails()) {
            return response($validator->errors(), 403);
        }
        $data['date_of_start'] = date('Y-m-d', strtotime($data['date_of_start']));



        if (!empty($data['created_at'])) {
            $isSaved = $project->find($data['id'])->fill($data)->update();
            $projectId = $data['id'];

            $oldProject = $project->where('id', $data['id'])->first();

        } else {
            $data['created_by'] = \Sentry::getUser()->id;
            $isSaved = $project->fill($data)->save();
            $project->fill(['pid' => $project->client_id . '-' . $project->id])->save();
            $projectId = $project->id;
            $oldProject = [];
        }

        $contractedHour = ContractedHour::where('project_id', '=', $projectId)->get();
        $woId = $data['work_order'];
        $this->saveContractedHours($data, $projectId , $woId);
        //making history log
        $this->logHistory($contractedHour, $projectId, $data, $oldProject);
        $this->saveProjectTags($data, $projectId);

        if ($isSaved) {
            return response()->json([
                'message' => 'Success',
                'project' => $projectId
            ], 200);
        }
        return response()->json([
            'message' => 'Error'
        ], 403);
    }

    public function projectList()
    {
        if (!\Sentry::getUser()->hasAccess('view_projects')) {
            return response()->json([
                'message' => "You don't have access to this area"
            ], 403);
        }
        return view('projects.project-list');
    }

    public function saveContractedHours($data, $projectId,$woId)
    {
        if (count($data['hours']) > 0) {
            foreach ($data['hours'] as $hour) {
                $hour['project_id'] = $projectId;

                ContractedHour::UpdateOrCreate(['resource_id' => $hour['resource_id'], 'project_id' => $hour['project_id'],
                    'work_order_id' => $woId],
                    [
                        'contracted_hour' => $hour['contracted_hour'],
                        'project_id' => $projectId,
                        'resource_id' => $hour['resource_id'],
                        'work_order_id' => $woId
                    ]);
            }
        }
    }

    public function saveProjectTags($data, $projectId)
    {
        if (empty($data['tags'])) return;
        //saving tags
        ProjectTag::where('project_id', $projectId)->delete();
        foreach ($data['tags'] as $tag) {
            if (empty($tag['id'])) continue;
            ProjectTag::create(['project_id' => $projectId, 'tag_id' => $tag['id']]);
        }
    }

    public function getProjectData($projectId)
    {
        $query = Project::with('client');
        $query->with('tags');
        $query->where('id', '=', $projectId);
        return $query->get();
    }

    public function getMaxId () {
        $id = DB::table('projects')->max('id')+1;
        return response($id, 200);
    }

    public function logHistory($contractedHour, $projectId, $data, $project)
    {
        if(!empty($project->work_order) && $project->work_order != $data['work_order']) {
            Projecthistory::create(
                [
                    'project_id' => $projectId,
                    'user_id' => \Sentry::getUser()->id,
                    'type' => 'work_order',
                    'resource_id' => '',
                    'action' => 'edit work order',
                    'old_content' => $project->work_order,
                    'new_content' => $data['work_order']
                ]
            );
        }

        $action = count($contractedHour) > 0 ? 'edit' : 'save';
        foreach ($data['hours'] as $row) {
            $isFound = false;
            if (empty($row['id'])) {
                Projecthistory::create(
                    [
                        'project_id' => $projectId,
                        'user_id' => \Sentry::getUser()->id,
                        'type' => 'contracted_hour',
                        'resource_id' => $row['resource_id'],
                        'action' => $action,
                        'old_content' => 0,
                        'new_content' => $row['contracted_hour']
                    ]
                );
                continue;
            }
            foreach ($contractedHour as $oldRow) {
                if ($oldRow->resource_id == $row['resource_id']) {
                    $isFound = true;
                    if ($oldRow->contracted_hour != $row['contracted_hour']) {
                        Projecthistory::create(
                            [
                                'project_id' => $projectId,
                                'user_id' => \Sentry::getUser()->id,
                                'type' => 'contracted_hour',
                                'action' => $action,
                                'resource_id' => $row['resource_id'],
                                'old_content' => $oldRow->contracted_hour,
                                'new_content' => $row['contracted_hour']
                            ]
                        );
                    }
                }
            }
        }
    }

    public function assignAccountToProject(Request $request) {
        $check = Project::where('id', $request->get('project'))->update(['pid'=>$request->get('account').'-'.$request->get('project'),'client_id'=> $request->get('account')]);
        if($check) {
            return response()->json('updated!', 200);
        }
        return response()->json('Error!', 403);
    }
    public function deleteProject($id)
    {
//        print_r($id);
        $getProject = Project::find($id);
        $deleteRecord = $getProject->delete();
        DB::table('work_orders')->where('project_id', '=', $id)->delete();
        if ($deleteRecord) {

            return response()->json([
                'message' => 'Success'
            ], 200);
        }
        return response()->json([
            'message' => 'Error'
        ], 403);
    }

}
