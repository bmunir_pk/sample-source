<?php

namespace App\Modules\Administrator\Controllers;

use App\Models\Admin;
use App\Models\AgentRole;
use App\Models\AsteriskCertmanMapping;
use App\Models\AsteriskCxpanelUsers;
use App\Models\AsteriskDevices;
use App\Models\AsteriskEndPointExtension;
use App\Models\AsteriskUsers;
use App\Models\CdrNumber;
use App\Models\Extension;
use App\Models\FreepbxUser;
use App\Models\VoicemailSetting;
use App\RedisModels\Agent;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class FreepbxUserController extends Controller
{
    public $viewDir = "administrator.freepbx_user";

    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            return FreepbxUser::findRequested();
        }
        return $this->view("index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge(['username' => $request->get('default_extension')]);
        $rules = FreepbxUser::validationRules();
        $adminRights = $request->get('admin_rights');
        if ($adminRights) {
            $rules['admin_role_id'] = 'required|integer';
        }
        $queueGroupExtensionRange = collect($this->queueGroupExtensionRange());
        $extensionRange = $queueGroupExtensionRange->whereLoose('title', $request->get('department'))->first();
        if ($extensionRange) {
            $extensionRange = $extensionRange['range'];
            $rules['default_extension'] = "required|integer|unique:freepbx_users|between:{$extensionRange['min']},{$extensionRange['max']}";
        }

        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails())
            return response($validator->errors(), 403);
        $data = $request->except(['admin_rights', 'admin_role_id']);
        $data['password'] = sha1(array_get($data, 'password'));
        if($request->get('bulk_upload') == 'false') {
            if(!empty($data['deskphone']['ext'])) {

                $extRecord = AsteriskEndPointExtension::where('ext', $data['deskphone']['ext'])->first();
                $isRecordExist = AsteriskEndPointExtension::where('mac', $data['deskphone']['mac'])->count();
                //if multiple mac address exist
                if ($isRecordExist > 1) return response('mac address already exists', 403);
                //new entry and mac already exists
                if ($isRecordExist == 1 && empty($extRecord)) return response('mac address already exists', 403);
                //max exist but associated to another one
                if ($isRecordExist == 1 && $extRecord->ext != $data['deskphone']['ext']) return response('mac address associated to another extension', 403);
            }
            //web rtc extension availablility
            if(!empty($data['webrtc']['ext'])) {
                $ifExists = AsteriskCertmanMapping::where('id', $data['webrtc']['ext'])->first();
                if($ifExists) { return response('Webrtc Extension already exists', 403); };
            }
            $dataNew = $data;

            $data['webrtc'] = !empty($data['webrtc']['ext']) ? $data['webrtc']['ext'] : '';
            $data['softphone'] = !empty($data['softphone']['ext']) ? $data['softphone']['ext'] : '';
            $data['deskphone'] = !empty($data['deskphone']['ext']) ? $data['deskphone']['ext'] : '';


            $csvResponse = $this->generateCsvAndUploadConfigurations($request->all());
            if(!$csvResponse) {
                return response('Webrtc Extension already exists', 403);
            }
            //Dump Webrtc entry in table.
            $this->saveWebrtcExtension($dataNew);

            //dump and configure Deskphone settings
            $this->saveDeskphoneandConfigurations($dataNew);

            //generate csv and upload config for DIDs
            //$this->generateCsvAndUploadDIDConfigurations($request->all());

            if ($adminRights) {
                $admin = Admin::create([
                    'name' => array_get($data, 'fname') . ' ' . array_get($data, 'lname'),
                    'email' => array_get($data, 'email'),
                    'extension' => array_get($data, 'username'),
                    'username' => array_get($data, 'username'),
                    'password' => md5($request->get('password')),
                    'status' => 'Enabled',
                    'admin_role_id' => $request->get('admin_role_id'),
                    'last_login' => date('Y-m-d H:i:s'),
                ]);
                if ($admin) {
                    $data['admin_id'] = $admin->id;
                }
            }
            $return = FreepbxUser::create($data);
            //for user settings
            $this->saveExtensionSettings($request->all(), $return);


            return $return;
        }
        else {
            return $this->bulkCreateUsers($request, $data);
        }
    }

    protected function saveDeskphoneandConfigurations($data) {
        if(empty($data['deskphone']['ext'])) {return;}

        $objAEPE = new AsteriskEndPointExtension();
        $data['deskphone']['account'] = 'account1';
        $objAEPE->fill($data['deskphone'])->save();
        AsteriskEndPointExtension::rebuildDeviceConfig($data['deskphone']['ext']);
    }

    protected function saveWebrtcExtension ($data) {
        if(empty($data['webrtc']['ext'])) {return;}
        AsteriskCertmanMapping::insert( [
            'id' => $data['webrtc']['ext'],
            'cid' => '2',
            'verify' => 'fingerprint',
            'setup' => 'actpass',
            'rekey' => 0
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function show(Request $request, FreepbxUser $freepbxUser)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function edit(Request $request, FreepbxUser $freepbxUser)
    {
        if ($request->wantsJson()) {
            $freepbxUser->load([
                'admin'
            ]);
            return $freepbxUser;
        }
        return $this->view("edit", ['id' => $freepbxUser->id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request $request
     * @return  \Illuminate\Http\Response
     */
    public function update(Request $request, FreepbxUser $freepbxUser)
    {
        if ($request->wantsJson()) {
            $attr = $request->get('name');
            $value = $request->get('value');
            if ($attr == 'password') {
                abort(403, "Editing the $attr is not allowed.");
            }
            $adminId = null;
            if($attr == 'admin_id' && !$value) {
                $adminId = $freepbxUser->admin_id;
            }
            $data = [$attr => $value];
            $validator = \Validator::make($data, FreepbxUser::validationRules($request->name));
            if ($validator->fails())
                return response($validator->errors()->first($request->name), 403);
            $freepbxUser->update($data);

            if($attr= "lname" || $attr = "fname") {
                $this->updatePbxDescription ($freepbxUser);
            }

            if ($adminId) {
                Admin::whereId($adminId)->delete();
                return "Admin rights updated";
            }
            $response = "User " . ucwords($attr) . " Updated.";
            return $response;
        }

        $this->validate($request, FreepbxUser::validationRules());
        $freepbxUser->update($request->all());
        return redirect('/freepbx-user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return  \Illuminate\Http\Response
     */
    public function destroy(Request $request, FreepbxUser $freepbxUser)
    {
        $freepbxUser->doLogoutActions();
        $this->doDeleteActions($freepbxUser->username);
        $freepbxUser->delete();
        return "User deleted";
    }

    private function doDeleteActions($username)
    {
        $keys = [
            "extensions",
            "agent-data",
        ];
        foreach ($keys as $key) {
            app("redis")->del("{$key}:{$username}");
        }
    }

    public function bulkDelete(Request $request)
    {
        abort(404);
        $items = $request->items;
        if (!$items) {
            abort(403, "Please select some items.");
        }

        if (!$ids = collect($items)->pluck('id')->all()) {
            abort(403, "No ids provided.");
        }

        FreepbxUser::whereIn('id', $ids)->delete();
        return response("Deleted");
    }

    protected function view($view, $data = [])
    {
        return view($this->viewDir . "." . $view, $data);
    }

    public function profilePic(Request $request, FreepbxUser $freepbxUser)
    {
        if (!$request->get('img')) {
            return response('Please provide image to upload.', 422);
        }
        //Profile pics Path
        $profilePicDirectory = '/agents';
        $profilePicsLocation = base_path() . '/public_html' . $profilePicDirectory;
        if (!file_exists($profilePicsLocation))
            \File::makeDirectory($profilePicsLocation, 0775, true);

        $imageData = base64_decode(preg_replace('/data\:image\/.+;base64\,/', '', $request->get('img')));
        $source = imagecreatefromstring($imageData);
        $agentProfilePicPath = $profilePicsLocation . '/' . $freepbxUser->username . '.jpg';
        $imageSave = imagejpeg($source, $agentProfilePicPath, 100);
        imagedestroy($source);
        if ($imageSave) {
            $freepbxUser->profile_pic = $profilePicDirectory . '/' . $freepbxUser->username . '.jpg';
            $freepbxUser->save();
            return $freepbxUser->profile_pic;
        } else {
            return response('Fail to upload image. Please try again', 422);
        }
    }

    public function exportCSV(Request $request)
    {
        $records = FreepbxUser::findRequested();
        exportCSV($records, 'Agents');
    }

    public function onlineAgents()
    {
        return Agent::all();
    }

    public function nextExtension()
    {
        $groupExtension = $this->queueGroupExtensionRange();
        foreach ($groupExtension as $key => $group) {
            $maxExtension = FreepbxUser::whereBetween(DB::raw("CONVERT(default_extension, UNSIGNED INTEGER)"), [
                $group['range']['min'],
                $group['range']['max']
            ])->max('default_extension');
            if ($maxExtension && $maxExtension < $group['range']['max'])
                $groupExtension[$key]['extension'] = ++$maxExtension;
            elseif (!$maxExtension)
                $groupExtension[$key]['extension'] = $group['range']['min'];
        }
        return $groupExtension;
    }

    /*
     * Advocate extensions restrictions
     * Digits: Near Shore are 4 and Domestic are 3
     * Starting point: Domestic is 370 and International is 6610.
     * May be range are: Near Shore 6610-9999 and Domestic is 370-999
     */
    private function queueGroupExtensionRange()
    {
        return [
            [
                'id' => 1,
                'title' => 'Domestic',
                'extension' => '',
                'range' => [
                    'min' => 370,
                    'max' => 999
                ]
            ],
            [
                'id' => 2,
                'title' => 'Near Shore',
                'extension' => '',
                'range' => [
                    'min' => 6610,
                    'max' => 9999
                ]
            ],
        ];
    }

    public function roles()
    {
        return AgentRole::all();
    }

    public function updatePassword(Request $request, FreepbxUser $freepbxUser)
    {
        $attr = 'password';
        $value = $request->get($attr);
        $validator = \Validator::make([$attr => $value], FreepbxUser::validationRules($attr));
        if ($validator->fails())
            return response($validator->errors()->first($attr), 403);
        $freepbxUser->$attr = sha1($value);
        $freepbxUser->save();
        return "User Password Updated.";
    }

    public function bulkCreateUsers ($request, $data)
    {
        if ($request->file('file')->getClientOriginalExtension() != 'csv') {
            return $this->uploadCustomError('The file must be a file of type CSV');
        }

        $requiredFields = FreepbxUser::$uploadFields;
        $columns = Excel::load($request->file('file'), function ($reader) {
            $reader->noHeading();
        })->first()->toArray();

        foreach ($requiredFields as $key => $field) {
            $requiredFields[$key]['index'] = array_search($field['label'], $columns);
        }
        $missingFields = collect($requiredFields)->where('index', false);
        if ($missingFields->count()) {
            $message = 'Invalid file format. ' . $missingFields->implode('label', ', ') . ' column(s) missing.';
            return $this->uploadCustomError($message);
        }
        $rows = [];
        $result = Excel::load($request->file('file'), function ($reader) {
            $reader->noHeading();
        })->skipRows(1)->get();
        $skipList = '';
        foreach ($result->toArray() as $row) {
            $userDetails = [];
            $userDetails['agent_role_id'] = $data['agent_role_id'];
            $userDetails['department'] = $data['department'];
            $userDetails['password'] = $data['password'];
            $userDetails['sip_password'] = $data['sip_password'];
            $userDetails['penalty'] = $data['penalty'];
            $userDetails['default_queues'] = $data['default_queues'];
            $userDetails['location_id'] = $data['location_id'];
            foreach ($requiredFields as $column => $field) {
                if($columns == 'extension') {
                    $userDetails['username'] = $row[$field['index']];
                    $userDetails['default_extension'] = $row[$field['index']];
                    unset($row[$field['index']]);
                    continue;
                }

                $userDetails[$column] = $row[$field['index']];
                unset($row[$field['index']]);
            }

            //check if extension already exists then skip it.
            if(FreepbxUser::where('username', $userDetails['username'])->count()){
                $skipList .= $userDetails['username'].', ';
                continue;
            }
            $rows[] = $userDetails;
        }
        FreepbxUser::insert($rows);
        $message = 'Users has been uploaded.';
        !empty($skipList) and $message .= ' following users already exists. '.$skipList;
        return $message;
    }

    public function bulkUpload () {
        return $this->view("bulk-upload");
    }

    private function uploadCustomError ($message)
    {
        return response(['file' => [$message]], 422);
    }

    private function copyCsvFileToPbx ($fileName) {
        $rootDir = '/var/spool/asterisk/bulk_uploads/';
        $pbxDisk = \Storage::disk('pbx');
        $publicDisk = \Storage::disk('public');
        $file = $publicDisk->get("extensions/".$fileName);
        if (!$file) return false;
        return $pbxDisk->put($rootDir.$fileName, $file);
    }

    private function loadConfigurationOfUser ($fileName, $type = 'extensions') {
        $action = new \PAMI\Message\Action\OriginateAction('Local/1@dummy');
        $action->setContext('dummy');
        //$action->set('dummy');
        $action->setExtension(1);
        $action->setApplication('TrySystem');
        $action->setData('fwconsole bi --type=\''.$type.'\' /var/spool/asterisk/bulk_uploads/'.$fileName);
        $action->setPriority(1);
        $action->setAsync('yes');
        app('ami')->send($action);

        $action = new \PAMI\Message\Action\OriginateAction('Local/1@dummy');
        $action->setContext('dummy');
        //$action->set('dummy');
        $action->setExtension(1);
        $action->setApplication('TrySystem');
        $action->setData('fwconsole reload');
        $action->setPriority(1);
        $action->setAsync('yes');
        $result = app('ami')->send($action);
        $response = $result->getKeys();
        \ChromePhp::log($response['response']);
        if($response['response'] == "Success") {
            return true;
        }
        return false;

    }

    private function generateCsvAndUploadConfigurations ($data) {

        Storage::disk('public')->put("extensions/".$data['username'].'.csv', implode(",", array_keys($this->getCsvHeaders())));
        !empty($data['webrtc']['ext']) and Storage::disk('public')->append("extensions/".$data['username'].'.csv', implode(",", array_values($this->getCsvRow('webrtc', $data['webrtc'], $data['fname'].' '.$data['lname']))));
        !empty($data['softphone']['ext']) and Storage::disk('public')->append("extensions/".$data['username'].'.csv', implode(",", array_values($this->getCsvRow('softphone', $data['softphone'], $data['fname'].' '.$data['lname']))));
        !empty($data['deskphone']['ext']) and Storage::disk('public')->append("extensions/".$data['username'].'.csv', implode(",", array_values($this->getCsvRow('deskphone', $data['deskphone'], $data['fname'].' '.$data['lname']))));
        $this->copyCsvFileToPbx($data['username'].'.csv');
        return $this->loadConfigurationOfUser($data['username'].'.csv');
    }

    private function generateCsvAndUploadDIDConfigurations ($data) {
        $file=$data['username'].'_'.time().'.csv';
        $fileName = "extensions/".$file;
        Storage::disk('public')->put($fileName, implode(",", array_keys($this->getCsvHeaders(true))));
        !empty($data['dids']['webrtc']['did']) and Storage::disk('public')->append($fileName, implode(",", array_values($this->getCsvRow('webrtc', $data['dids']['webrtc'], $data['fname'].' '.$data['lname'], true, $data['webrtc']['ext']))));
        !empty($data['dids']['softphone']['did']) and Storage::disk('public')->append($fileName, implode(",", array_values($this->getCsvRow('softphone', $data['dids']['softphone'], $data['fname'].' '.$data['lname'], true, $data['softphone']['ext']))));
        !empty($data['dids']['deskphone']['did']) and Storage::disk('public')->append($fileName, implode(",", array_values($this->getCsvRow('deskphone', $data['dids']['deskphone'], $data['fname'].' '.$data['lname'], true, $data['deskphone']['ext']))));
        $this->copyCsvFileToPbx($file);
        $this->loadConfigurationOfUser($file, 'dids');
    }

    private function getCsvHeaders ($isDid=false) {
        if($isDid) {
            return [
                'id' => '',
                'cidnum' => '',
                'extension'	 => '',
                'destination'  => '',
                'privacyman' => '',
                'alertinfo' => '',
                'ringing' => '',
                'mohclass' => '',
                'description' => '',
                'grppre' => '',
                'delay_answer' => '',
                'pricid' => '',
                'pmmaxretries' => '',
                'pmminlength' => '',
                'reversal' => '',
                'rvolume' => '',
                'callrecording' => '',
                'fax_enable' => '',
                'fax_detection' => '',
                'fax_detectionwait' => '',
                'fax_destination' => '',
            ];
        }
        return [
            "extension"=> '',
            "name" => '',
            "voicemail"=> 'novm',
            "mohclass" => 'default',
            "tech" => 'sip',
            "recording_in_external" => 'no',
            "recording_out_external" => 'no',
            "recording_in_internal" => 'no',
            "recording_out_internal" => 'no',
            "avpf" => 'yes',
            "encryption" => 'yes',
            "force_avp" => 'yes',
            "icesupport" => 'yes',
            "nat" => 'no',
            "secret" => '1234pccw',
            "transport" => 'wss,udp,tcp,tls',
            "trustrpid" => 'yes',
            "videosupport" => 'no',
            "rtcp_mux" => 'yes',
            "callwaiting_enable" => '',
            "voicemail_enable" => '',
            "disable_star_voicemail" => '',
            'mailbox'=> 'ext$@device', // ---example 999123@device or blank
            'voicemail_vmpwd' => '', //numeric password for voicemail
            'voicemail_email' => '', //email address for notifications
            'voicemail_options' => ''// attach=yes|delete=yes or attach=no|delete=no
        ];
    }

    private function getCsvRow($phoneType, $data, $name, $isDid = false, $ext=false) {
        if($isDid) {
            return [
                'id' => '',
                'cidnum' => '',
                'extension'	 => $data['did'],
                'destination'  => 'from-did-direct,'.$ext.',1',
                'privacyman' => '0',
                'alertinfo' => '',
                'ringing' => '',
                'mohclass' => 'default',
                'description' => $name,
                'grppre' => '',
                'delay_answer' => '0',
                'pricid' => '',
                'pmmaxretries' => '',
                'pmminlength' => '',
                'reversal' => '',
                'rvolume' => '',
                'callrecording' => 'dontcare',
                'fax_enable' => '',
                'fax_detection' => '',
                'fax_detectionwait' => '',
                'fax_destination' => '',
            ];
        }

        $valueBasedOnWebRTC = $phoneType == 'webrtc' ? 'yes' : 'no';
        $transport = $phoneType == 'webrtc' ? 'wss,udp,tcp,tls' : 'udp,tcp,tls';
        $recording = !empty($data['recording']) && $data['recording'] == 1 ? 'yes' : 'no';
        $enableVoiceMail = !empty($data['enable_voicemail']) && $data['enable_voicemail'] == 1 ? 'yes' : '';
        $others = ['attach=no', 'saycid=no', 'envelope=yes', 'delete=no'];
        if(!empty($data['voicemail'])) {
            $others = ['saycid=no', 'envelope=yes'];
            foreach ($data['voicemail']['others'] as $key => $value) {
                $string = $key . '=';
                $string .= $value == 1 ? 'yes' : 'no';
                $others[] = $string;

            }
        }
        $others = implode("|", $others);
        return [
            "extension"=> $data['ext'],
            "name" => $name,
            "voicemail"=> 'novm',
            "mohclass" => 'default',
            "tech" => 'sip',
            "recording_in_external" => $recording,
            "recording_out_external" => $recording,
            "recording_in_internal" => $recording,
            "recording_out_internal" => $recording,
            "avpf" => $valueBasedOnWebRTC, //yes for web else no
            "encryption" => $valueBasedOnWebRTC, //yes for web else no
            "force_avp" => $valueBasedOnWebRTC, //yes for web else no
            "icesupport" => $valueBasedOnWebRTC, //yes for web else no
            "nat" => 'no',
            "secret" => '1234pccw',
            "transport" => '"'.$transport.'"',
            "trustrpid" => 'yes',
            "videosupport" => 'no',
            "rtcp_mux" => 'yes',
            "callwaiting_enable" => '',
            "voicemail_enable" => $enableVoiceMail,
            "disable_star_voicemail" => $enableVoiceMail,
            'mailbox'=> !empty($enableVoiceMail) ? $data['ext'].'@device' : '', // ---example 999123@device or blank
            'voicemail_vmpwd' => !empty($enableVoiceMail) ? !empty($data['voicemail']['password']) ? $data['voicemail']['password'] : '' : '', //numeric password for voicemail
            'voicemail_email' => !empty($enableVoiceMail) ? !empty($data['voicemail']['email']) ? $data['voicemail']['email'] : '' : '', //email address for notifications
            'voicemail_options' => !empty($enableVoiceMail) ? $others : ''
        ];
    }

    public function assignDidsOnPanel ($data) {
        foreach ($data['dids'] as $key => $setting) {
            CdrNumber::where('number', $setting['did'])->update(['assigned_to_extension'=> $data[$key]['ext']]);
        }
    }

    public function updatePbxDescription ($freepbxUser) {
        $extensions = [];
        !empty($freepbxUser->deskphone) and  $extensions[] = $freepbxUser->deskphone;
        !empty($freepbxUser->webrtc) and  $extensions[] = $freepbxUser->webrtc;
        !empty($freepbxUser->softphone) and  $extensions[] = $freepbxUser->softphone;
        if(!empty($extensions)) {
            foreach ($extensions as $ext) {
                AsteriskDevices::where('user', $ext)->update(['description'=> $freepbxUser->fname.' '. $freepbxUser->lname]);
                AsteriskCxpanelUsers::where('user_id', $ext)->update(['display_name'=> $freepbxUser->fname.' '. $freepbxUser->lname]);
                AsteriskUsers::where('extension', $ext)->update(['name'=> $freepbxUser->fname.' '. $freepbxUser->lname]);
            }
        }
    }

    protected function saveExtensionSettings ($data, $user) {
        $extensions = [];
        !empty($data['deskphone']) and  $extensions['deskphone'] = $data['deskphone'];
        !empty($data['webrtc']) and  $extensions['webrtc'] = $data['webrtc'];
        !empty($data['softphone']) and  $extensions['softphone'] = $data['softphone'];
        foreach ($extensions as $key=>$extension) {
            if(empty($extension['ext'])) continue;
            $extension['user_id'] = $extension['voicemail']['user_id'] =$user->id;
            $extension['type'] = $key;
            $extension['extension'] =  $extension['ext'];
            $extension['mac_address'] = !empty($extension['mac']) ? $extension['mac'] : '';
            $extension['enable_recording'] = !empty( $extension['recording']) ? $extension['recording'] : 0;
            $extension['sip_password'] = $key == 'webrtc' ? rand(10001,99999) : '';
            $extObj = new Extension();
            $extObj->fill($extension)->save();
            if(!empty($extension['enable_voicemail']) && $extension['enable_voicemail'] == 1) {
                $vmObj = new VoicemailSetting();
                $extension['voicemail']['extension'] = $extension['extension'];
                if (!empty($extension['voicemail']['others'])) {
                    $extension['voicemail']['attachment'] = !empty($extension['voicemail']['others']['attach']) ? $extension['voicemail']['others']['attach'] : 'no';
                    $extension['voicemail']['delete_attachment'] = !empty($extension['voicemail']['others']['delete']) ? $extension['voicemail']['others']['delete'] : 'no';
                } else {
                    $extension['voicemail']['attachment'] = 'no';
                    $extension['voicemail']['delete_attachment'] = 'no';
                }
                $vmObj->fill($extension['voicemail'])->save();
                /*
                 * Blocked code to aovid voicemail conf manual update.
                if ($vmObj->getConfig($extension['voicemail'], $user)) {
                    $response = $vmObj->saveConfiguration($extension['voicemail'], $user);
                } else {
                    $response = $vmObj->saveConfiguration($extension['voicemail'], $user, 'Append');
                }*/
            }
            //$this->updateExtensionConfig($extension);

        }
    }

    protected function updateExtensionConfig ($data) {
        $this->putDBActionValues('AMPUSER/'.$data['ext'], 'recording/in/internal', $data['recording']);
        $this->putDBActionValues('AMPUSER/'.$data['ext'], 'recording/out/internal', $data['recording']);
        $this->putDBActionValues('AMPUSER/'.$data['ext'], 'recording/in/external', $data['recording']);
        $this->putDBActionValues('AMPUSER/'.$data['ext'], 'recording/out/external', $data['recording']);
    }

    protected function putDBActionValues ($family, $key, $value) {
        $action = new \PAMI\Message\Action\DBPutAction($family, $key, $value);
        $response = app('ami')->send($action);
        $check = $response->getEvents();
        if(count($check) == 0 || !$check) return '';
        $responseArray = $check[0]->getKeys();
        return $responseArray['val'];
    }

    public function updateUsername(Request $request, FreepbxUser $freepbxUser) {
        $isExists = FreepbxUser::where('username', $request->get('value'))
                                ->where('id', '<>',$freepbxUser->id)
                                ->count();
        if($isExists>0) return response('Username already taken', 403);
        $freepbxUser->username = $request->get('value');
        $freepbxUser->save();
        return response('Updated!', 200);
    }

    /**
     * Assign admin rights to user.
     * @param $request Request
     * @param $freepbxUser FreepbxUser
     * @return  \Illuminate\Http\Response
     */
    public function assignAdminRights(Request $request, FreepbxUser $freepbxUser)
    {
        if (!$adminRoleId = $request->get('admin_role_id')) {
            abort(400, 'Admin role is required.');
        }
        $admin = Admin::whereUsername($freepbxUser->username)->first();
        if (!$admin) {
            $admin = new Admin();
            $admin->last_login = date('Y-m-d H:i:s');
        }
        $admin->name = $freepbxUser->fullName();
        $admin->username = $admin->extension = $freepbxUser->username;
        $admin->admin_role_id = $adminRoleId;
        $admin->email = $freepbxUser->email;
        $admin->password = $freepbxUser->password;
        $admin->status = 'Enabled';
        $admin->save();
        $freepbxUser->admin_id = $admin->id;
        $freepbxUser->save();
        return response("Saved");
    }
}
