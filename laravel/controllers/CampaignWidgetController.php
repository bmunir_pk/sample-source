<?php

namespace App\Modules\Front\Controllers;

use App\Helpers\CampaignHelper;
use App\Http\Controllers\Controller;
use App\Models\CdrLocation;
use App\Models\DontCallNumberList;
use App\RedisModels\Agent;
use App\RedisModels\Extension;
use Illuminate\Http\Request;
use App\Models\Campaign;
use App\Models\CampaignNumber;
use PAMI\Message\Action\OriginateAction;

use Redis;

class CampaignWidgetController extends Controller
{
    public function loadCampaigns()
    {

        $query = Campaign::query();
        $query->where('parent_campaign_id', 0);
        $query->where('status', 1);
        $query->where('is_sms_campaign', 0);
        $query->where('campaign_type', '<>', 'Agent Less');
        $query->where('start_date', '<=', date('Y-m-d'));
        $query->where(function ($query) {
            $query->where('repeat', '=', 'One Time');
            $query->orWhere(function ($query) {
                $query->where('repeat', '!=', 'One Time');
                $query->where('start_date', '<=', date('Y-m-d'));
                $query->where('end_date', '>=', date('Y-m-d'));
            });
        });
        $relations = [
            'schedule',
            'days',
            'dispositions',
        ];
        $campaigns = $query->with($relations)->get();
        $filteredCampaigns = [];
        foreach ($campaigns as $campaign) {
            if ($campaign->days->count()) {
                $days = $campaign->days->filter(function ($day) {
                    return $day->report_day == date('l') && $day->status == 1;
                });
                if (!$days->count())
                    continue;
            }
            $filteredCampaigns[] = $campaign;
        }
        return $filteredCampaigns;
    }

    private function removeAssignmentFromOtherCampaigns($currentCampaignId)
    {
        return CampaignNumber::where([
            ['dial_status', '=', 0],
            ['agent_id', '=', \Auth::id()],
            ['campaign_id', '!=', $currentCampaignId],
        ])->update(['agent_id' => null]);
    }

    public function toggleStatus()
    {
        if (CampaignHelper::toggleStatus(\Auth::user()) !== false) {
            return response('status changed successfully');
        }
        return response('Error while changing status. Reload page.', 400);
    }

    public function loadCampaignNumber(Campaign $campaign)
    {
        if (CampaignHelper::isAgentPaused(\Auth::user()->username)) return;
        $this->removeAssignmentFromOtherCampaigns($campaign->id);
        $locationCode = null;
        if ($campaign->location_id) {
            $location = CdrLocation::whereId($campaign->location_id)->first();
            if ($location && $location->location_code) {
                $locationCode = $location->location_code;
            }
        }

        return $this->loadNextNumber($campaign, $locationCode);
    }

    /**
     * @param $campaignId integer
     * @param $redialNumber boolean | false
     * @return CampaignNumber
     */
    private function getCampaignNumber($campaignId, $redialNumber = false)
    {
        return CampaignNumber::where([
            ['campaign_id', $campaignId],
            ['do_not_call', 0],
            ['date_time', '=', null],
            ['parent_id', $redialNumber ? '!=' : '=', 0],
            ['call_status', null]
        ])->where(function ($query) {
            $query->where('agent_id', '=', \Auth::id());
            $query->orWhere('agent_id', '=', null);
        })->orderBy('agent_id', 'DESC')//Let's load already assigned number to agent in priority
            ->first();
    }

    /**
     * @param $campaign Campaign
     * @param $locationCode
     * @return mixed
     */
    private function loadNextNumber($campaign, $locationCode)
    {
        $number = $this->getCampaignNumber($campaign->id);
        if (!$number) {//Get redial number
            $number = $this->getCampaignNumber($campaign->id, true);
        }

        if (!$number) {
            $this->changeCampaignStatus($campaign);
            $agent = Agent::find(\Auth::user()->username);
            if ($agent) {
                $agent->save([
                    'campaignNumberIdAssigned' => '',
                ]);
            }
            return [];
        }
        $number->load('number');
        $dontCall = DontCallNumberList::findNumber($number->number->number);
        if (!$dontCall) {
            //Assign number to agent first
            $number->assignNumber();
            $number = $number->load(['info']);
            $number->number->code = $locationCode;
            return $number;
        }
        $number->do_not_call = 1;
        $number->call_status = "Don't Call";
        $number->save();
        return $this->loadNextNumber($campaign, $locationCode);
    }

    /**
     * @param $campaign Campaign
     */
    private function changeCampaignStatus($campaign)
    {
        //Check if any call remaining
        $number = CampaignNumber::where([
            ['campaign_id', '=', $campaign->id],
            ['do_not_call', '=', 0],
            ['call_status', '=', 0],
        ])->count();
        //change status if all numbers are dispositioned
        if (!$number) {
            $campaign->completed_at = date('Y-m-d H:i:s');
            $campaign->status = 0;
            $campaign->save();
            app('pusher')->trigger('campaigns', 'removed', $campaign->toArray());
        }
    }

    public function dispositionCampaignNumber(Request $request, CampaignNumber $campaignNumber)
    {
        $user = \Auth::user();
        $lastCallEntry = $user->lastCallEntry();
        $data = [
            'call_status' => $request->get('call_status'),
            'do_not_call' => $request->get('do_not_call') ? 1 : 0,
            'comments' => $request->get('comments'),
            'dial_status' => 1,
            'agent_id' => $user->id,
            'unique_id' => $lastCallEntry ? $lastCallEntry->call_id : NULL
        ];
        $campaignNumber->update($data);
        Redis::del($campaignNumber->campaign_id . '_' . $campaignNumber->number->number);
        if ($request->get('do_not_call')) {
            DontCallNumberList::create([
                'agent_id' => $user->id,
                'number_id' => $campaignNumber->number_id,
                'number' => $campaignNumber->number->number,
            ]);
        }
        return "Saved";
    }

    public function callThroughHardPhone(Request $request)
    {
        $this->validate($request,
            ['caller_id' => 'required'],
            ['caller_id.required' => 'Caller id is required.']
        );
        $callerId = $callerExtension = $request->get('caller_id');
        $callerName = $request->get('caller_name');
        if ($request->get('extension_prefix')) {
            $callerExtension = $request->get('extension_prefix') . $callerExtension;
        }

        $user = \Auth::user();
        $action = new OriginateAction("Local/{$user->username}@from-internal");
        $action->setContext("from-internal");
        $action->setExtension($callerExtension);
        $action->setPriority(1);
        $action->setCallerId("{$callerName} <{$callerId}>");
        $action->setTimeout("30000");
        $action->setAsync(true);
        $extension = Extension::find($user->username);
        if ($extension && $extension->auto_answer && $extension->auto_answer == 'Enabled') {
            $action->setVariable("__SIPADDHEADER", "Alert-Info: \;Alertinfo=Auto Answer");
        }
        $res = app('ami')->send($action);
        if (!$res->isSuccess()) {
            abort(400, "Error: " . $res->getMessage());
        }
        return response('You should receive a call on your phone shortly.');
    }

    public function loadDataForCallScript(Request $request)
    {
        $data = [];
        $agent_id = $request->get('agent_id');
        $campaignId = $request->get('campaign_id');
        if ($agent_id && $campaignId) {
            $number = CampaignNumber::where([
                ['campaign_id', $campaignId],
                ['agent_id', $agent_id],
                ['call_status', null]
            ])->orderBy('agent_id', 'DESC')
                ->with(['number', 'info'])
                ->first();
            if ($number) {
                $data = [
                    'FNAME' => $number->number->first_name,
                    'LNAME' => $number->number->last_name,
                    'caller_id' => "{$number->number->last_name} {$number->number->last_name}",
                    'caller_id_num' => $number->number->number,
                ];
                foreach ($number->info as $metaData) {
                    $data[$metaData->key] = $metaData->value;
                }
            }
        }
        $data['greeting'] = $this->getGreeting();
        return $data;
    }

    private function getGreeting()
    {
        $hour = date("H");
        if ($hour < 12) {
            return "good morning";
        } elseif ($hour > 11 && $hour < 18) {
            return "good afternoon";
        } elseif ($hour > 17) {
            return "good evening";
        }
    }
}