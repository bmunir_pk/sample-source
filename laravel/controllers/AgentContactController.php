<?php

namespace App\Modules\Front\Controllers;

use App\Http\Controllers\Controller;
use App\Models\SipContact;
use App\Models\SipContactNumber;
use App\Models\SipContactsFavorite;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class AgentContactController extends Controller
{
    public function loadFavoriteContacts()
    {
        return SipContactsFavorite::whereAgentId(\Auth::id())->with('contact')->get();
    }

    public function contactToggleFavorite(SipContact $sipContact)
    {
        if ($sipContact->isFavorite) {
            SipContactsFavorite::where([['agent_id', \Auth::id()], ['contact_id', $sipContact->id]])->delete();
            return "Removed from favorite";
        } else {
            $fav = SipContactsFavorite::create([
                'agent_id' => \Auth::id(),
                'contact_id' => $sipContact->id
            ]);
            return $fav->load('contact');
        }
    }

    public function import(Request $request)
    {
        if (!$groupId = $request->get('group_id'))
            return response("Contact group field is required.", 422);

        if (!$request->hasFile('file'))
            return response("Please select file", 422);

        if ($request->file('file')->getClientOriginalExtension() != 'csv')
            return response("The file must be a file of type CSV", 422);

        $columns = Excel::load($request->file('file'), function ($reader) {
            $reader->noHeading();
        })->first()->toArray();

        $contactTypes = SipContactNumber::contactTypes();
        if (!in_array('Name', $columns) || !array_intersect($contactTypes, $columns)) {
            return response("Invalid File", 422);
        }
        $contactDetailsTypes = [];
        foreach ($contactTypes as $type) {
            $index = array_search($type, $columns);
            if ($index) {
                $contactDetailsTypes[$index] = $type;
            }
        }
        $result = Excel::load($request->file('file'), function ($reader) {
            $reader->noHeading();
        })->skipRows(1)->get();

        $nameIndex = array_search("Name", $columns);
        $companyIndex = array_search("Company", $columns);
        $notesIndex = array_search("Notes", $columns);
        foreach ($result->toArray() as $row) {
            $contact = [
                'name' => $row[$nameIndex],
                'company' => array_get($row, $companyIndex),
                'notes' => array_get($row, $notesIndex),
                'group_id' => $groupId
            ];
            $details = [];
            foreach ($contactDetailsTypes as $index => $type) {
                if ($number = array_get($row, $index)) {
                    array_push($details, [
                        'contact_type' => $type,
                        'value' => $number
                    ]);
                }
            }

            if (count($details)) {
                $contact = SipContact::create($contact);
                $contact->details()->createMany($details);
            }
        }
        return response("Contact import successfully");
    }
}
