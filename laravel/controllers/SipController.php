<?php

namespace App\Modules\Front\Controllers;

use App\Http\Controllers\Controller;
use App\Models\CampaignNumber;
use App\RedisModels\Agent;
use App\Traits\HasLogFile;
use PAMI\Message\Action\ParkAction;
use PAMI\Message\Action\StatusAction;

class SipController extends Controller
{
    use HasLogFile;
    private $logFile = 'phone-actions';

    public function loadCampaignNumberInfo()
    {
        return CampaignNumber::with(['info','number','campaign'])->whereId(request('number'))->first();
    }

    public function saveTransferInfo()
    {
        $query = CampaignNumber::join('phone_numbers', 'campaign_numbers.number_id', '=', 'phone_numbers.id')
            ->select("phone_numbers.*", "campaign_numbers.*")
            ->whereAgentId(\Auth::user()->id)
            ->whereNull('call_status');
        $campaignNumber = numberQuery($query, 'phone_numbers.number', request('customerNum'))->first();

        if (!$campaignNumber) {
            return 'Not on campaign';
        }

        return app('redis')->set("transfer-info." . request('destination') . "." . request('callerId'), $campaignNumber->id);
    }

    public function parkCaller()
    {
        $action = new StatusAction();
        $res = app('ami')->send($action);
        $events = $res->getEvents();
        $numberToPark = request('number');
        $parkingLot = request('parkingLot');
        $channelToPark = null;

        foreach ($events as $event) {
            if ($numberToPark == $event->getKey('calleridnum')) {
                $channelToPark = $event->getKey('channel');
            }
        }

        if (!$channelToPark) {
            abort(400, 'No channel to park.');
        }

        $action = new ParkAction($channelToPark, '', false, $parkingLot);
        $res = app('ami')->send($action);

        if (!$res->isSuccess()) {
            abort(400, $res->getMessage());
        }

        return "Successfully parked";
    }

    /*
     * Mute / unMute a Mixmonitor recording.
     * https://wiki.asterisk.org/wiki/display/AST/Asterisk+13+ManagerAction_MixMonitorMute
     * */
    public function toggleRecordingMonitor()
    {
        $user = \Auth::user();
        if (!$user->canPauseRecording()) {
            $this->log("{$user->username} user is not authorized");
            abort(400, 'Sorry, you are not authorized for this action.');
        }
        $agent = Agent::find($user->username);
        if (!$agent) {
            $this->log("{$user->username} agent not found." . toStr($agent));
            abort(400, 'Sorry, you are not authorized for this action.');
        }

        if ($agent->recordingMonitorStatus == 'Mute') {
            $status = 'Un-Mute';
            $agent->completeMuteRecording();
        } else {
            $status = 'Mute';
            $agent->startMuteRecording();
        }

        return "Recording {$status}d";
    }
}