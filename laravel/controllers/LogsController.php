<?php

namespace App\Http\Controllers;

use App\Models\ContractedHour;
use App\Models\HoursSpent;
use App\Models\Project;
use App\Models\ProjectTag;
use App\Models\Resource;
use App\Models\WorkOrder;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Models\Projecthistory;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class LogsController extends Controller
{
    public function loadHistory(Request $request, $projectId) {
        $data = Projecthistory::where('project_id', $projectId)
            ->with('user')
            ->with('resource')
            ->orderBy('id', 'DESC')
            ->skip(0)->take(100)
            ->get();
        return response()->json($data, 200);
    }

    public function loadSpentHours (Request $request, Project $project, WorkOrder $work_order) {
        $resources = ContractedHour::where('project_id', $project->id)
            ->where('work_order_id', $project->work_order)
            ->join('resources', 'resources.id', '=', 'contracted_hours.resource_id')
            ->select('resources.id', 'resources.type')
            ->get();
        $hours = ContractedHour::where('project_id', $project->id)
            ->where('work_order_id', $project->work_order)
            ->pluck('contracted_hour', 'resource_id');
        $spentHours = $this->getSpentHours($project->id, $resources, $project->work_order);
        return response()->json(
            [
                'resources'=>$resources,
                'hours'=> $hours,
                'spent_hours' => $spentHours['spent_hours'],
                'total_spenthours'=> $spentHours['total_spent_hours']
            ], 200);
    }

    public function saveSpentHours(Request $request) {
        $check = HoursSpent::where('resource_id', $request->get('resource_id'))
            ->where('project_id', $request->get('project_id'))
            ->where('work_order_id', $request->get('work_order_id'))
            ->where('logged_date', date('Y-m-d', $request->get('date')))
            ->first();
        if($check) {
            HoursSpent::where('id', $check->id)->update([
                'hours' => $request->get('hour'),
                'logged_by' => \Sentry::getUser()->id,
            ]);
            Projecthistory::create(
                [
                    'project_id' => $request->get('project_id'),
                    'work_order_id' => $request->get('work_order_id'),
                    'user_id' => \Sentry::getUser()->id,
                    'type' => 'spent_hour',
                    'action' => 'update',
                    'resource_id' => $request->get('resource_id'),
                    'old_content' => $check->hours,
                    'new_content' => $request->get('hour')
                ]
            );
            return response()->json('updated', 200);
        }
        HoursSpent::create([
           'resource_id' => $request->get('resource_id'),
            'work_order_id' => $request->get('work_order_id'),
           'project_id' => $request->get('project_id'),
           'hours' => $request->get('hour'),
           'logged_by' => \Sentry::getUser()->id,
           'logged_date' => date("Y-m-d", $request->get('date')),
        ]);
        Projecthistory::create(
          [
              'project_id' => $request->get('project_id'),
              'user_id' => \Sentry::getUser()->id,
              'type' => 'spent_hour',
              'action' => 'save',
              'resource_id' => $request->get('resource_id'),
              'old_content' => 0,
              'new_content' => $request->get('hour')
          ]
        );
        return response()->json('saved', 200);
    }

    public function CreateSpentHours (Request $request) {
        $data = $request->all();
        //check if given hours length is atleast 1;
        if(empty($data['hours']) || count($data['hours']) <= 0) {
            return response()->json('Fill in hour for atleast one resource first', 403);
        }
        $conflicts = [];
        foreach ($data['hours'] as $resource=>$hour) {
            //check if record exists already
            $check = HoursSpent::where('logged_date', date("Y-m-d", strtotime($data['date'])))
                ->where('resource_id',$resource)
                ->where('project_id', $data['project_id'])
                ->where('work_order_id', $data['work_order_id'])
                ->first();
            if($check) {
                $resourceType = Resource::where('id', $resource)->pluck('type')->first();
                $conflicts[] = 'Record for '.$resourceType.' already exists for selected date';
                continue;
            }
            HoursSpent::create([
                'resource_id' => $resource,
                'work_order_id' => $data['work_order_id'],
                'project_id' => $data['project_id'],
                'hours' => $hour,
                'logged_by' => \Sentry::getUser()->id,
                'logged_date' => date("Y-m-d", strtotime($data['date'])),
            ]);
            Projecthistory::create(
                [
                    'project_id' => $data['project_id'],
                    'user_id' => \Sentry::getUser()->id,
                    'type' => 'spent_hour',
                    'action' => 'save',
                    'resource_id' => $resource,
                    'old_content' => 0,
                    'new_content' => $hour
                ]
            );
        }
        return response()->json(['message'=>'saved', 'conflicts'=>$conflicts], 200);
    }

    protected function getSpentHours($projectId, $resources, $workOrderId) {
        $today = strtotime(date('m/d/Y', time()));
        $hours = HoursSpent::where('project_id', $projectId)->where('work_order_id', $workOrderId)->get();
        $spentHours = [];
        $totalSpentHours = [];
        if($hours) {
            foreach ($hours as $hour) {
                $dateChunk = explode(" ", $hour->logged_date);
                $date = strtotime($dateChunk[0]);
                if (!isset($spentHours[$date])) {
                    $spentHours[$date] = [];
                }
                if (!isset($totalSpentHours[$hour->resource_id])) {
                    $totalSpentHours[$hour->resource_id] = 0;
                }
                $spentHours[$date]['enabledEditing'] = false;
                empty($spentHours[$date][$hour->resource_id]) and $spentHours[$date][$hour->resource_id] = 0;
                $spentHours[$date][$hour->resource_id] = $hour->hours ? $spentHours[$date][$hour->resource_id] + $hour->hours : $spentHours[$date][$hour->resource_id] + 0;
                $totalSpentHours[$hour->resource_id] = $totalSpentHours[$hour->resource_id] + $hour->hours;
            }
        }

        if(!isset($spentHours[strtotime(date('m/d/Y', time()))])) {
            $spentHours[$today] = [];
            foreach ($resources as $resource) {
                $totalSpentHours[$today][$resource->id] = 0;
            }
        }
        $spentHours[$today]['enabledEditing'] = true;
        return ['spent_hours' => $spentHours, 'total_spent_hours'=>$totalSpentHours];
    }

}
