<?php

namespace App\Modules\Administrator\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\RedisModels\Extension;
use Illuminate\Http\Request;

class RealTimeExtensionStatusController extends Controller
{
    public function index()
    {
        return view('administrator.real-time-extension-status.real-time-extension-status');
    }

    public function load()
    {
        return app('ami')->getExtensionsFromPbx();
    }

    public function getAllowedExtensions()
    {
        $admin = \Auth::guard('admin')->user(); /* @var $admin Admin */
        return $admin->extensionsAllowed();
    }
}
